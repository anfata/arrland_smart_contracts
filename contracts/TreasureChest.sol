pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "hardhat/console.sol";

import "./RandomARRC.sol";

contract TreasureChest is ERC721URIStorage, Ownable {
    using Strings for uint256;
    using SafeMath for uint256;

    struct Chest {
        uint256 level;
        string state;
    }

	struct TokenInfo {
		uint256 tokenId;
		string state;
		uint256 level;
	}

    mapping(uint256 => Chest) public treasureChests;
    bytes32 public merkleRoot;
    uint256 public maxSupply;
    mapping(uint256 => uint256) public levelCap;
    mapping(uint256 => uint256) public levelUpPrice;
    mapping(uint256 => uint256) public levelCount;
    mapping(address => uint256[]) public ownedTokens;
    mapping(uint256 => uint256) public chestGemLevels;
    mapping(address => bool) public isMinted;
    RandomARRC public randomARRC;
    uint256 public totalSupply;
    address public admin;
    bool public isPublicMintingEnabled;
    bool public isWhitelistMintingEnabled;
    bool public isOpenEnabled;
    address private arrcTokenAddress;
    uint8 public constant MAX_LEVEL = 8;
    string public baseURI;

    event Minted(uint256 indexed tokenId, address user);
    event ChestLeveledUp(uint256 indexed tokenId, address user, uint256 level, uint256 price);
    event ChestOpened(uint256 indexed tokenId, address user, uint256 level);

    constructor(
        string memory _baseURIpath
    ) ERC721("Arrlandum Hidden Gem", "ArrHG") {
        baseURI = _baseURIpath;
        totalSupply = 0;
        maxSupply = 3333;
        levelCap[1] = 3333;
        levelCap[2] = 1500;
        levelCap[3] = 1000;
        levelCap[4] = 550;
        levelCap[5] = 200;
        levelCap[6] = 50;
        levelCap[7] = 10;
        levelCap[8] = 23;
        chestGemLevels[1] = 1;
        chestGemLevels[2] = 2;
        chestGemLevels[3] = 3;
        chestGemLevels[4] = 3;
        chestGemLevels[5] = 4;
        chestGemLevels[6] = 4;
        chestGemLevels[7] = 5;
        chestGemLevels[8] = 5;
        isPublicMintingEnabled = false;
        isWhitelistMintingEnabled = false;
        isOpenEnabled = false;
        admin = msg.sender;
    }

    function initializeLevelUpPrice(uint256[] memory _levelUpPrices) external onlyOwner {
        for (uint256 i = 0; i < _levelUpPrices.length; i++) {
            levelUpPrice[i + 2] = _levelUpPrices[i];
        }
    }
    modifier onlyAdmin() {
        require(msg.sender == admin || msg.sender == owner(), "Caller is not admin or owner");
        _;
    }
    function setAdmin(address _admin) external onlyOwner {
        admin = _admin;
    }

    function setbaseURI(string memory _baseURIpath) external onlyOwner {
        baseURI = _baseURIpath;
    }

    function mint(bytes32[] calldata merkleProof) external {
        require(totalSupply < maxSupply, "Max supply reached");
        require(!isMinted[msg.sender], "You can only mint once");

        if (isWhitelistMintingEnabled) {
            bytes32 node = keccak256(abi.encodePacked(msg.sender));
            require(MerkleProof.verify(merkleProof, merkleRoot, node), "Invalid proof");
        } else {
            require(isPublicMintingEnabled, "Public minting is not enabled");
        }

        isMinted[msg.sender] = true;
        levelCount[1]++;
        totalSupply++;
        treasureChests[totalSupply] = Chest(1, "chest");
        _mint(msg.sender, totalSupply);
        ownedTokens[msg.sender].push(totalSupply);

        emit Minted(totalSupply, msg.sender);
    }

    function levelUp(uint256 tokenId, uint256 targetLevel) external payable {
        require(_exists(tokenId), "Token ID does not exist");
        require(ownerOf(tokenId) == msg.sender, "Caller is not token owner");
        require(keccak256(abi.encodePacked(treasureChests[tokenId].state)) != keccak256(abi.encodePacked("gem")), "Chest is already opened");

        Chest storage chest = treasureChests[tokenId];
        uint256 currentLevel = chest.level;
        require(targetLevel <= MAX_LEVEL, "Max level is 8");
        require(currentLevel < MAX_LEVEL, "Max level reached");
        require(currentLevel < targetLevel, "Target level should be higher than current level");

        require(levelCount[targetLevel] < levelCap[targetLevel], "Next level is fully minted");
        uint256 totalPrice = levelUpPrice[targetLevel]-levelUpPrice[currentLevel];
        require(msg.value == totalPrice, "Payment must be exact");

        for (uint256 level = currentLevel + 1; level <= targetLevel; level++) {
            levelCount[level]++;
            levelCount[level - 1]--;
        }

        chest.level = targetLevel;

        emit ChestLeveledUp(tokenId, msg.sender, targetLevel, msg.value);
    }

    function open(uint256 tokenId) external {
        require(isOpenEnabled, "Open phase not started");
        require(ownerOf(tokenId) == msg.sender, "Caller is not owner");
        require(keccak256(abi.encodePacked(treasureChests[tokenId].state)) != keccak256(abi.encodePacked("gem")), "Chest is already opened");
        treasureChests[tokenId].state = "gem";

        if (treasureChests[tokenId].level > 1 && randomARRC != RandomARRC(address(0))) {
            randomARRC.requestRandomNumber(msg.sender, tokenId, treasureChests[tokenId].level);
        }

        emit ChestOpened(tokenId, msg.sender, treasureChests[tokenId].level);

    }

    function setRandomARRCAddress(address randomARRCAddress) external onlyOwner {
        randomARRC = RandomARRC(randomARRCAddress);
    }

	function withdraw(address withdrawAddress) external onlyOwner {
		uint256 balance = address(this).balance;
		require(balance > 0);
		payable(withdrawAddress).transfer(balance);
	}

    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
        Chest storage chest = treasureChests[tokenId];
		if (keccak256(abi.encodePacked(treasureChests[tokenId].state)) != keccak256(abi.encodePacked("gem"))){
			return string(abi.encodePacked(_baseURI(), chest.state, "/", chest.level.toString(), "/", tokenId.toString()));
		}
	    return string(abi.encodePacked(_baseURI(), "gem", "/", chestGemLevels[chest.level].toString(), "/", tokenId.toString()));

    }

    function _transfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override {
        super._transfer(from, to, tokenId);

        // Remove token from `from` address
        uint256[] storage fromTokens = ownedTokens[from];
        for (uint256 i = 0; i < fromTokens.length; i++) {
            if (fromTokens[i] == tokenId) {
                fromTokens[i] = fromTokens[fromTokens.length - 1];
                fromTokens.pop();
                break;
            }
        }
        // Add token to `to` address
        ownedTokens[to].push(tokenId);
    }
    function getOwnedTokens(address user) public view returns (uint256[] memory) {
        return ownedTokens[user];
    }

	function updateContractSettings(bool publicMintingEnabled, bool whitelistMintingEnabled, bool openEnabled) external onlyAdmin {
		isPublicMintingEnabled = publicMintingEnabled;
		isWhitelistMintingEnabled = whitelistMintingEnabled;
		isOpenEnabled = openEnabled;
	}

    function updateMerkleRoot(bytes32 _merkleRoot) external onlyAdmin {
        merkleRoot = _merkleRoot;
    }

	function getLevelInfo() public view returns (uint256[] memory prices, uint256[] memory counts, bool publicMintingEnabled, bool whitelistMintingEnabled, bool openEnabled) {
		prices = new uint256[](MAX_LEVEL - 1);
		counts = new uint256[](MAX_LEVEL);
		for (uint256 i = 2; i <= MAX_LEVEL; i++) {
			prices[i - 2] = levelUpPrice[i];
		}
		for (uint256 i = 1; i <= MAX_LEVEL; i++) {
			counts[i - 1] = levelCount[i];
		}
		publicMintingEnabled = isPublicMintingEnabled;
		whitelistMintingEnabled = isWhitelistMintingEnabled;
		openEnabled = isOpenEnabled;
		return (prices, counts, publicMintingEnabled, whitelistMintingEnabled, openEnabled);
	}

	function getTokenInfoList(address user) public view returns (TokenInfo[] memory) {
        uint256[] memory tokenIds = getOwnedTokens(user);
        TokenInfo[] memory tokenInfoList = new TokenInfo[](tokenIds.length);
        for (uint256 i = 0; i < tokenIds.length; i++) {
            uint256 tokenId = tokenIds[i];
            Chest storage chest = treasureChests[tokenId];
            tokenInfoList[i] = TokenInfo(tokenId, chest.state, chest.level);
        }
        return tokenInfoList;
    }
}