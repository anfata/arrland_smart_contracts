// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@chainlink/contracts/src/v0.8/interfaces/VRFCoordinatorV2Interface.sol";
import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";
import "@chainlink/contracts/src/v0.8/ConfirmedOwner.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Context.sol";

contract RandomARRC is VRFConsumerBaseV2, ConfirmedOwner {

    bytes32 internal keyHash;
    uint256[][] public levelRewards;
    mapping(uint256 => uint256) private requestIdToTokenId;
    mapping(uint256 => address) private requestIdToUser;
    mapping(uint256 => uint256) private requestIdToLevel;
    address private treasureChestAddress;
    address private arrcTokenAddress;
    IERC20 public arrc;

    event RequestRandomNumber(uint256 indexed tokenId, address user, uint256 indexed level, uint256 requestId);
    event RandomNumberReceived(uint256 indexed tokenId, address user, uint256 indexed requestId, uint256 randomNumber, uint256 reward);

	struct RequestStatus {
        bool fulfilled; // whether the request has been successfully fulfilled
        bool exists; // whether a requestId exists
        uint256[] randomWords;
    }
    mapping(uint256 => RequestStatus)
        public s_requests; /* requestId --> requestStatus */
    VRFCoordinatorV2Interface COORDINATOR;

    // Your subscription ID.
    uint64 s_subscriptionId;

    // past requests Id.
    uint256[] public requestIds;
    uint256 public lastRequestId;

    // The gas lane to use, which specifies the maximum gas price to bump to.
    // For a list of available gas lanes on each network,
    // see https://docs.chain.link/docs/vrf/v2/subscription/supported-networks/#configurations


    uint32 callbackGasLimit;

    // The default is 3, but you can set this higher.
    uint16 requestConfirmations = 3;

    // For this example, retrieve 2 random values in one request.
    // Cannot exceed VRFCoordinatorV2.MAX_NUM_WORDS.
    uint32 numWords = 1;

    constructor(
        address _vrfCoordinator,
        uint64 _subscriptionId,
		bytes32 _keyHash,
        uint32 _callbackGasLimit,
        address _treasureChestAddress,
        address _arrcTokenAddress
    )
        VRFConsumerBaseV2(_vrfCoordinator)
        ConfirmedOwner(msg.sender)
    {
        COORDINATOR = VRFCoordinatorV2Interface(
            _vrfCoordinator
        );
        keyHash = _keyHash;
        s_subscriptionId = _subscriptionId;
        callbackGasLimit = _callbackGasLimit;
        treasureChestAddress = _treasureChestAddress;
        arrcTokenAddress = _arrcTokenAddress;
        arrc = IERC20(_arrcTokenAddress);
		levelRewards = new uint256[][](8); // Set the size of the first dimension to 8 (for 8 levels)

    	levelRewards[0] = [0.06 * (10 ** 18), 0.12 * (10 ** 18), 0.25 * (10 ** 18), 4 * (10 ** 18)];   // Rewards for level 2
    	levelRewards[1] = [0.012 * (10 ** 18), 0.25 * (10 ** 18), 0.5 * (10 ** 18), 8 * (10 ** 18)];  // Rewards for level 3
    	levelRewards[2] = [0.25 * (10 ** 18), 0.5 * (10 ** 18), 1 * (10 ** 18), 15 * (10 ** 18)];     // Rewards for level 4
    	levelRewards[3] = [0.5 * (10 ** 18), 1 * (10 ** 18), 2 * (10 ** 18), 25 * (10 ** 18)];        // Rewards for level 5
    	levelRewards[4] = [0.75 * (10 ** 18), 1.5 * (10 ** 18), 3 * (10 ** 18), 35 * (10 ** 18)];     // Rewards for level 6
    	levelRewards[5] = [1 * (10 ** 18), 2 * (10 ** 18), 4 * (10 ** 18), 50 * (10 ** 18)];          // Rewards for level 7
    	levelRewards[6] = [2 * (10 ** 18), 4 * (10 ** 18), 8 * (10 ** 18), 100 * (10 ** 18)];          // Rewards for level 8
    }

    function requestRandomNumber(address _user, uint256 _tokenId, uint256 _level) external returns (uint256 requestId) {
        require(msg.sender == treasureChestAddress, "Only TreasureChest contract can call this function");

        requestId = COORDINATOR.requestRandomWords(
            keyHash,
            s_subscriptionId,
            requestConfirmations,
            callbackGasLimit,
            numWords
        );
        s_requests[requestId] = RequestStatus({
            randomWords: new uint256[](0),
            exists: true,
            fulfilled: false
        });
        requestIds.push(requestId);
        lastRequestId = requestId;

        requestIdToTokenId[requestId] = _tokenId;
        requestIdToUser[requestId] = _user;
        requestIdToLevel[requestId] = _level;
        emit RequestRandomNumber(_tokenId, _user, _level, requestId);

        return requestId;
    }

    function fulfillRandomWords(uint256 _requestId, uint256[] memory _randomWords) internal override {
    	require(s_requests[_requestId].exists, "request not found");
        s_requests[_requestId].fulfilled = true;
        s_requests[_requestId].randomWords = _randomWords;
    	uint256 tokenId = requestIdToTokenId[_requestId];
    	uint256 level = requestIdToLevel[_requestId];
		address user = requestIdToUser[_requestId];
        uint256 randomResult = _randomWords[0];

        uint256 randomPercentage = randomResult % 100;
        uint256 reward;

        if (randomPercentage < 70) {
            reward = levelRewards[level - 2][0];
        } else if (randomPercentage < 90) {
            reward = levelRewards[level - 2][1];
        } else if (randomPercentage < 99) {
            reward = levelRewards[level - 2][2];
        } else {
            reward = levelRewards[level - 2][3];
        }

        // Transfer the calculated reward to the owner of the token
        arrc.transfer(user, reward);

        emit RandomNumberReceived(tokenId, user, _requestId, randomResult, reward);
    }

    function withdraw() external onlyOwner {
		uint256 contractBalance = arrc.balanceOf(address(this));
        require(arrc.transfer(msg.sender, contractBalance), "Token transfer failed");
    }

    function setSettings(bytes32 _keyHash, uint32 _callbackGasLimit) external onlyOwner {
        callbackGasLimit = _callbackGasLimit;
        keyHash = _keyHash;
    }

    function getRequestStatus(
        uint256 _requestId
    ) external view returns (bool fulfilled, uint256[] memory randomWords) {
        require(s_requests[_requestId].exists, "request not found");
        RequestStatus memory request = s_requests[_requestId];
        return (request.fulfilled, request.randomWords);
    }
}
