// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TestContract {
    ERC20 private _token;

    constructor(address token) {        
        _token = ERC20(token);
    }

    function testPayment(uint256 value) external {
        _token.transferFrom(msg.sender, address(this), value);        
    }
}
