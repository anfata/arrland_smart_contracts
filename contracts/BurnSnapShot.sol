

// SPDX-License-Identifier: MIT

// More inforamtion about the RUM-GEMPAD token
// https://arrland.com/gempad

pragma solidity ^0.8.16;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol"; 
import "@openzeppelin/contracts/access/Ownable.sol";

contract BurnSnapShot is ERC20, Ownable { 
    bool public transferBlock;
    mapping(address => bool) public userExist;
    address[] public holders;

    event TokensBurn(
        address user,
        uint256 tokenCount
    );

    constructor(address pool_creator, address owner_transfer) ERC20("RUM-GEMPAD", "RUM-GP") {
        _mint(pool_creator, 3758305 * 10 ** decimals());
        transferOwnership(owner_transfer);
    }

    function setTransferBlock(bool isBlock) public onlyOwner {
        transferBlock = isBlock;
    }
     function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        require(!transferBlock, "Transfer is Blocked");
        super._transfer(from, to, amount);
    } 

    function totalNumberOfHolder() external view returns(uint256) {
        return holders.length;
    }

    function burnSingle(address holder) external onlyOwner {
        uint256 tokenCount = balanceOf(holder);
        _burn(holder, tokenCount);

        emit TokensBurn(
            holder,
            tokenCount
        );
    }

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 amount)
        internal
        override(ERC20)
    {
        super._beforeTokenTransfer(from, to, amount);
        if(from != address(0) && !userExist[from])
        {
            userExist[from] = true;
            holders.push(from);
        }
        if(to != address(0) && !userExist[to])
        {
            userExist[to] = true;
            holders.push(to);
        }
    }

}