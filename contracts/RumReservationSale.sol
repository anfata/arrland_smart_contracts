pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/MerkleProofUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/IERC20MetadataUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";


contract RumReservationSale is OwnableUpgradeable, PausableUpgradeable {
	using SafeMathUpgradeable for uint256;

    event TokensReservation(address userAddress, uint256 date, uint256 amountTransfered, uint256 rumTokens, uint256 price, bool isWhitelisted);
    event MekleRootAdded(bytes32 root);

    struct Purchase {
        address userAddress;
        uint256 date;
        uint256 amountTransfered;
        uint256 rumTokens;
        uint256 price;
        bool isWhitelisted;
    }

    uint256 public MAX_USDC_PER_WALLET;
    uint256 public WHITELISTED_PRICE;
    uint256 public PUBLIC_PRICE;
    IERC20MetadataUpgradeable public usdc;
    mapping(address => uint256) public totalUsdcTransfer;
    mapping(address => Purchase[]) public userPurchases;
    mapping(address => bool) public hasUserMadePurchase;
    address[] public allPurchasingAddresses;
    mapping(bytes32 => bool) public merkleRoots;
    address public withdrawAddress;
    bytes32 public lastMerkleRoot;

    function initialize(address _usdc, address withdrawAddress_) external initializer {
        __Ownable_init();
        __Pausable_init();
        usdc = IERC20MetadataUpgradeable(_usdc);
        MAX_USDC_PER_WALLET = 1000 * (10 ** usdc.decimals());
		WHITELISTED_PRICE = 23 * (10 ** usdc.decimals()); // 0.023 in wei
		PUBLIC_PRICE = 24 * (10 ** usdc.decimals()); // 0.024 in wei
        withdrawAddress = withdrawAddress_;
    }

    function whiteListUser(bytes32 root) external onlyOwner {
        merkleRoots[root] = true;
        lastMerkleRoot = root;
        emit MekleRootAdded(root);
    }

	function isWhitelisted(address account, bytes32[] memory proof) external view returns(bool) {
	    require(lastMerkleRoot != bytes32(0), "No Merkle root has been set.");
		bytes32 leaf = keccak256(abi.encodePacked(account));
		return MerkleProofUpgradeable.verify(proof, lastMerkleRoot, leaf);
	}

	function buyRumToken(uint256 amountOfUsdc, bytes32[] memory proof) external whenNotPaused {
	    require(lastMerkleRoot != bytes32(0), "No Merkle root has been set.");
		bytes32 leaf = keccak256(abi.encodePacked(msg.sender));
		bool isWhitelisted = MerkleProofUpgradeable.verify(proof, lastMerkleRoot, leaf);
		uint256 totalPurchaseByUser = totalUsdcTransfer[msg.sender];
		require(totalPurchaseByUser + amountOfUsdc <= MAX_USDC_PER_WALLET, "Exceeds limit.");
		uint256 rumTokens;
		uint256 price;

		if (isWhitelisted) {
			price = WHITELISTED_PRICE;
		} else {
			price = PUBLIC_PRICE;
		}
		rumTokens = amountOfUsdc.mul(10 ** usdc.decimals()).div(price);
		require(usdc.transferFrom(msg.sender, address(this), amountOfUsdc));
		totalUsdcTransfer[msg.sender] += amountOfUsdc;

		Purchase memory newPurchase = Purchase({
			userAddress: msg.sender,
			date: block.timestamp,
			amountTransfered: amountOfUsdc,
			rumTokens: rumTokens,
			price: price,
			isWhitelisted: isWhitelisted
		});

		userPurchases[msg.sender].push(newPurchase);
		if (!hasUserMadePurchase[msg.sender]) {
            hasUserMadePurchase[msg.sender] = true;
            allPurchasingAddresses.push(msg.sender);
        }
		emit TokensReservation(msg.sender, block.timestamp, amountOfUsdc, rumTokens, price, isWhitelisted);
	}

    function setPrices(uint256 newWhitelistedPrice, uint256 newPublicPrice) external onlyOwner {
        require(
            newWhitelistedPrice % (10 ** usdc.decimals()) == 0,
            "Whitelisted price is not in the correct format."
        );
        require(
            newPublicPrice % (10 ** usdc.decimals()) == 0,
            "Public price is not in the correct format."
        );
        WHITELISTED_PRICE = newWhitelistedPrice;
        PUBLIC_PRICE = newPublicPrice;
    }

    function getPurchasesByUser(address user) external view returns (Purchase[] memory) {
        return userPurchases[user];
    }

    function getAllPurchasingAddresses() external view returns (address[] memory) {
        return allPurchasingAddresses;
    }

    function withdrawUsdcToken()
        external
        onlyOwner
    {
        usdc.transfer(withdrawAddress, usdc.balanceOf(address(this)));
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unPause() public onlyOwner {
        _unpause();
    }
}
