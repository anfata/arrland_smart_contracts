// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";

contract ARRCTokenClaimer is Ownable {
    IERC20 public arrcToken;
    address public admin;
    bytes32 public merkleRoot;

    mapping(bytes32 => bool) public claimed;

    event Claim(address indexed user, uint256 amount, uint256 index);

    constructor(IERC20 _arrcToken) {
        arrcToken = _arrcToken;
        admin = msg.sender;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin || msg.sender == owner(), "Caller is not admin or owner");
        _;
    }

    function setAdmin(address _admin) external onlyOwner {
        admin = _admin;
    }

    function setMerkleRoot(bytes32 _merkleRoot) external onlyAdmin {
        merkleRoot = _merkleRoot;
    }

    function claimTokens(uint256 _amount, uint256 _index, bytes32[] memory _proof) external {
    	uint256 contractBalance = arrcToken.balanceOf(address(this));
	    require(contractBalance >= _amount, "Not enough tokens in the contract");
        bytes32 leaf = keccak256(abi.encodePacked(msg.sender, _amount, _index));
        require(MerkleProof.verify(_proof, merkleRoot, leaf), "Invalid Merkle proof");
        require(!claimed[leaf], "Tokens already claimed");

        claimed[leaf] = true;

        arrcToken.transfer(msg.sender, _amount);

        emit Claim(msg.sender, _amount, _index);
    }

    function withdrawUnclaimedTokens(uint256 _amount) external onlyOwner {
		uint256 contractBalance = arrcToken.balanceOf(address(this));
	    require(contractBalance >= _amount, "Withdrawal amount exceeds unclaimed tokens");
        require(arrcToken.transfer(msg.sender, _amount), "Token transfer failed");
    }
}
