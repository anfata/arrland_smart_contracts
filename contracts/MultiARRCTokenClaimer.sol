// SPDX-License-Identifier: MIT

pragma solidity 0.8.17;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";


contract MultiARRCTokenClaimer is Ownable {
    using SafeERC20 for IERC20;

    address public admin;

    IERC20 public arrc;

	mapping(uint256 => bytes32) public merkleRoots;
    mapping(uint256 => uint256) public totalAmounts;
    mapping(uint256 => mapping(address => bool)) public claimed;

    event Claimed(uint256 indexed distributionId, address indexed account, uint256 amount);
    event DistributionAdded(uint256 indexed distributionId, bytes32 merkleRoot, uint256 totalAmount);
	event DistributionRemoved(uint256 indexed distributionId);

    constructor(address arrc_) {
        arrc = IERC20(arrc_);
        admin = msg.sender;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin || msg.sender == owner(), "Caller is not admin or owner");
        _;
    }

    function setAdmin(address _admin) external onlyOwner {
        admin = _admin;
    }

	function addDistribution(uint256 distributionId, bytes32 merkleRoot, uint256 totalAmount) external onlyAdmin {
        merkleRoots[distributionId] = merkleRoot;
        totalAmounts[distributionId] = totalAmount;
        emit DistributionAdded(distributionId, merkleRoot, totalAmount);
    }

    function claim(uint256 distributionId, uint256 amount, bytes32[] calldata merkleProof) external {
        // Verify the distribution exists
        require(totalAmounts[distributionId] > 0, "Invalid distributionId");

        // Verify the merkle proof.
        bytes32 leaf = keccak256(abi.encodePacked(distributionId, msg.sender, amount));
        require(MerkleProof.verify(merkleProof, merkleRoots[distributionId], leaf), "Invalid proof");

         // Ensure the token is not claimed yet.
        require(!claimed[distributionId][msg.sender], "Already claimed");

        // Ensure the amount to claim does not exceed the total distribution amount or the total contract balance.
        require(amount <= totalAmounts[distributionId], "Claim exceeds total distribution amount");
        require(amount <= arrc.balanceOf(address(this)), "Claim exceeds contract's ARRC token balance");

        // Mark it as claimed and send the token.
        claimed[distributionId][msg.sender] = true;

        arrc.safeTransfer(msg.sender, amount);

        emit Claimed(distributionId, msg.sender, amount);
    }

    function removeDistribution(uint256 distributionId) external onlyAdmin {
		require(merkleRoots[distributionId] != 0, "Distribution not found");
		delete merkleRoots[distributionId];
		delete totalAmounts[distributionId];
		emit DistributionRemoved(distributionId);
	}

	function withdrawUnclaimedTokens(uint256 _amount) external onlyOwner {
		uint256 contractBalance = arrc.balanceOf(address(this));
	    require(contractBalance >= _amount, "Withdrawal amount exceeds unclaimed tokens");
        require(arrc.transfer(msg.sender, _amount), "Token transfer failed");
    }


}
