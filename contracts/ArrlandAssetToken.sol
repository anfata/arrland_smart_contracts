pragma solidity ^0.8.17;
import "@openzeppelin/contracts/utils/structs/EnumerableMap.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ArrlandAssetToken is Ownable, ERC20Burnable {
    using SafeMath for uint256;

    using EnumerableMap for EnumerableMap.AddressToUintMap;

    // Declare a set state variable
    EnumerableMap.AddressToUintMap private mintTimestamps;

    uint public burnDelay;

    constructor(string memory name, string memory symbol, uint _burnDelay) ERC20(name, symbol){
        burnDelay = _burnDelay;
    }

    function decimals() public view virtual override returns (uint8) {
        return 0;
    }

    function getAllHolders() public view returns (address[] memory) {
        address[] memory holders = new address[](mintTimestamps.length());
        uint holderCount = 0;
        for (uint j = 0; j < mintTimestamps.length(); j++) {
            
            (address holder, uint256 ts) = mintTimestamps.at(j);
            if (balanceOf(holder) > 0) {
                holders[holderCount] = holder;
                holderCount++;
            }
        }
        return holders;
    }

    function burn_all_tokens() public onlyOwner {
        address[] memory holders = getAllHolders();
        for (uint i = 0; i < holders.length; i++) {
            if (mintTimestamps.contains(holders[i]) && block.timestamp >= mintTimestamps.get(holders[i]) + burnDelay) {
                _burn(holders[i], balanceOf(holders[i]));
            }
        }
    }

    function redistribute(address[] memory addresses, uint[] memory counts) public onlyOwner {
        require(addresses.length == counts.length, "Addresses and counts arrays must have the same length.");
        for (uint i = 0; i < addresses.length; i++) {
            mint(addresses[i], counts[i]);
            mintTimestamps.set(addresses[i], block.timestamp);
        }
    }

    function mint(address to, uint value) public onlyOwner {
        _mint(to, value);
        mintTimestamps.set(to, block.timestamp);
    }

    function _updateTimestamps(address from, address to, uint256 value, uint256 senderBalance) internal {
        mintTimestamps.set(to, mintTimestamps.get(from));
        
        if (senderBalance == value) {
            mintTimestamps.set(from, 0);
        }
    }

    function transferFrom(address from, address to, uint256 value) public override returns (bool) {
        uint256 senderBalance = balanceOf(from);
        super.transferFrom(from, to, value);
        _updateTimestamps(from, to, value, senderBalance);
        return true;
    }

    function transfer(address to, uint256 value) public override returns (bool) {
        uint256 senderBalance = balanceOf(msg.sender);
        super.transfer(to, value);
        _updateTimestamps(msg.sender, to, value, senderBalance);
        return true;
    }

    function getMintTimestamp(address user) public view returns (uint256) {
        return mintTimestamps.get(user);
    }
}
