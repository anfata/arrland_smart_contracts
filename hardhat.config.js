require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-ethers");
require('@openzeppelin/hardhat-upgrades');
require('hardhat-contract-sizer');
require("hardhat-gas-reporter");
require("@nomiclabs/hardhat-web3");
require('dotenv').config()



const defaultNetwork = "localhost";

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const dev = process.env.DEV_PRIVATE_KEY;
const prod = process.env.PROD_PRIVATE_KEY;
const dev_rpc_matic = process.env.DEV_RPC_MATIC;
const dev_rpc_mumbai = process.env.DEV_RPC_MUMBAI;
const dev_rpc_goreli = process.env.DEV_RPC_GOREELI;
const dev_rpc_bsc = process.env.DEV_RPC_BSC;
const prod_rpc_eth = process.env.PROD_RPC_ETH;
const etherscan_api_key = process.env.ETHERSCAN_API_KEY;
const bsc_api_key = process.env.BSC_API_KEY;
const matic_api_key = process.env.MATIC_API_KEY;
const prod_rpc_matic = process.env.PROD_RPC_MATIC;
const prod_rpc_bsc = process.env.PROD_RPC_BSC;
/**
 * @type import('hardhat/config').HardhatUserConfig
 */


module.exports = {
  solidity: {
    version: "0.8.17",
    compilers: [
      {
        version: '0.8.17',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.0',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },//0.8.2
      {
        version: '0.8.2',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.1',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      }
    ],
  },
  gasReporter: {
    currency: 'USD',
    gasPrice: 100,
    coinmarketcap: "d264d887-d985-43ae-8c5d-bb6de12af3a9"
  },
  settings: {
    optimizer: {
      enabled: true,
      runs: 150,
    },
  },
  networks: {
    mumbai: {
      url: dev_rpc_mumbai,
      accounts: dev ? [dev] : dev,
      gas: "auto"
    },
    matic: {
      url: prod_rpc_matic,
      accounts: prod ? [prod] : prod,
      gas: "auto",
      gasPrice: 190000000000,
      timeout: 2000000000
    },
    goerli: {
      url: dev_rpc_goreli,
      accounts: dev ? [dev] : dev,
      //chainId: 5,
      //maxPriorityFeePerGas: 9000000000, //units of gas you are willing to pay, aka gas limit
      //gasPrice:  38290424261, //gas is typically in units of gwei, but you must enter it as wei here
      //maxFeePerGas: 66528967074,
      //timeout: 200000000000
    },
    bsctestnet: {
      url: 'https://data-seed-prebsc-2-s2.binance.org:8545',
      accounts: dev ? [dev] : dev,
    },
    bsc: {
      url: prod_rpc_bsc,
      accounts: prod ? [prod] : prod,
    },
    eth: {
     url: prod_rpc_eth,
     accounts: prod ? [prod] : prod,
     gas: "auto",
     gasPrice: 17000000000,
     timeout: 2000000000
    },
  },
  etherscan: {
    //apiKey: bsc_api_key
    //apiKey: etherscan_api_key
    apiKey: matic_api_key
  },
  mocha: {
    timeout: 200000000
  }
};
