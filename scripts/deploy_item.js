


const { ethers, upgrades } = require("hardhat");



async function main() {
  // This is just a convenience check
  if (network.name === "hardhat") {
    console.warn(
      "You are trying to deploy a contract to the Hardhat Network, which" +
        "gets automatically created and destroyed every time. Use the Hardhat" +
        " option '--network localhost'"
    );
  }





  // ethers is avaialble in the global scope
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  console.log("Account balance:", (await deployer.getBalance()).toString());
  console.log("Network:", network.name);
  const ArrlandItem = await ethers.getContractFactory("ArrlandItem");
  
  //dev 

  const instance = await ArrlandItem.deploy("0x7917edb51ecd6cdb3f9854c3cc593f33de10c623", "https://arrland.app/");    
  
  // main net

  //const instance = await ArrlandItem.deploy("0x5FDCCA53617f4d2b9134B29090C87D01058e27e9", "https://arrland.app/");    
  
  
  await instance.deployed();

    console.log("ArrlandItem deployed to:", instance.address);
    console.log("npx hardhat verify  --network "+network.name+ " " + instance.address + " 0x7917edb51ecd6cdb3f9854c3cc593f33de10c623 https://arrland-media.s3-eu-central-1.amazonaws.com/meta/piratesV2/l1/");

    //npx hardhat verify  --network mainnet --contract contracts/ArrlandItem.sol:ArrlandItem 0x00Ed5f674Cd4A340bd8023dc1Ffb0dcf840FbF41 0x5FDCCA53617f4d2b9134B29090C87D01058e27e9 https://arrland.app/
}



main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


