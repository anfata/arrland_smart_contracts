const { ethers, upgrades } = require('hardhat');

async function main () {


  const currentImp = '0x06bB095402040390A78E9919f2a45A5c0046b0B4'; // current proxy not imp

  const ArrLandNFTV2 = await ethers.getContractFactory('ArrLandNFTv2');
  console.log('Upgrading ArrLandNFTV2...');
  const instance = await upgrades.upgradeProxy(currentImp, ArrLandNFTV2);
  console.log('ArrLandNFTV2 upgraded');

  console.log(instance.deployTransaction.hash)
  console.log("ArrLandNFTv2 deployed to:", instance.address);

  await instance.setImx("0x4527BE8f31E2ebFbEF4fCADDb5a17447B27d2aef");
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });