const { ethers } = require("hardhat");

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function main() {
    const [deployer] = await ethers.getSigners();
    const feeData = await ethers.provider.getFeeData();


    console.log("maxFeePerGas", feeData.maxFeePerGas.toString());
    console.log("gasPrice", feeData.gasPrice.toString());
    console.log("maxPriorityFeePerGas", feeData.maxPriorityFeePerGas.toString());

    let imx;
    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    if (network.name == "eth") {
        imx = "0x5fdcca53617f4d2b9134b29090c87d01058e27e9"
    } else {
        imx = "0x7917edb51ecd6cdb3f9854c3cc593f33de10c623"
    }



    await new Promise(resolve => {
        rl.question(`Are you sure you want deploy ? \n\n imx: ${imx} \n(y/n)`, async (answer) => {
            if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {

                const factory = await ethers.getContractFactory("ArrlandShip");

                console.log("deploy starting....");

                const contract = await factory.deploy(imx, "https://arrland-media.s3-eu-central-1.amazonaws.com/meta/ships/l1/");
                await contract.deployed();

                console.log("Deployed at: ", contract.address);

                const verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/ArrlandShip.sol:ArrlandShip ${imx} "https://arrland-media.s3-eu-central-1.amazonaws.com/meta/ships/l1/"`;
                console.log("Verify cmd is:");
                console.log(verifyCmd);

            } else {
                console.log("deploy cancelled");
            }
            rl.close();
            resolve();
        });
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

    // npx hardhat verify  --network goerli 0x6a6C9b05176253F54A8C5c579CA54C076Fc7aeAA --contract contracts/ArrlandIsland.sol:ArrlandIsland 0x7917edb51ecd6cdb3f9854c3cc593f33de10c623 "https://arrland-media.s3-eu-central-1.amazonaws.com/meta/islands/l1/"