


const { ethers, upgrades } = require("hardhat");



async function main() {
  // This is just a convenience check
  if (network.name === "hardhat") {
    console.warn(
      "You are trying to deploy a contract to the Hardhat Network, which" +
        "gets automatically created and destroyed every time. Use the Hardhat" +
        " option '--network localhost'"
    );
  }





  // ethers is avaialble in the global scope
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  console.log("Account balance:", (await deployer.getBalance()).toString());
  console.log("Network:", network.name);
  const ArrlandAsset = await ethers.getContractFactory("ArrlandAsset");
  
  //dev 

  //const instance = await ArrlandAsset.deploy("0x4527BE8f31E2ebFbEF4fCADDb5a17447B27d2aef", "https://arrland.app/assets86734L1/");    
  
  // main net

  const instance = await ArrlandAsset.deploy("0x5FDCCA53617f4d2b9134B29090C87D01058e27e9", "https://ipfs.moralis.io:2053/ipfs/QmfT7GxfxYRgbREj3BCsoovoNZeUtUVNWkRPoDrWHcHUaW/");    
  
  
  await instance.deployed();

  console.log("ArrlandAsset deployed to:", instance.address);
}



main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


