const { ethers } = require("hardhat");

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function main() {
    const [deployer] = await ethers.getSigners();
    let pool_creator;
    let owner_transfer;
    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    const factory = await ethers.getContractFactory("BurnSnapShot");

    if (network.name == "bsctestnet") {
        pool_creator = "0x5e5570cbD0aF664cC1b46F8CD61EC82d509721D3"
        owner_transfer = "0x5e5570cbD0aF664cC1b46F8CD61EC82d509721D3"
    } else {
        pool_creator = "0x15ac79e4f4a174DDA2d24cce8ce51f2dEE974Ad5"
        owner_transfer = "0x03A9FA0CcFD2A1978A09F62366A997387e2E5D36"
    }

    await new Promise(resolve => {
        rl.question(`Are you sure you want deploy ? \n pool_creator: ${pool_creator} \n owner_transfer: ${owner_transfer}\n\n(y/n)`, async (answer) => {
            if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {
                console.log("deploy starting");

                const contract = await factory.deploy(pool_creator, owner_transfer);

                console.log("Deployed at: ", contract.address);

                const verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/BurnSnapShot.sol:BurnSnapShot ${pool_creator} ${owner_transfer}`;
                console.log("Verify cmd is:");
                console.log(verifyCmd);

            } else {
                console.log("deploy cancelled");
            }
            rl.close();
            resolve();
        });
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
