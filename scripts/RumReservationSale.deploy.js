const { ethers, upgrades } = require("hardhat");
const { getAdminAddress } = require('@openzeppelin/upgrades-core');
const { getImplementationAddress } = require('@openzeppelin/upgrades-core');

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// utils/verifyContract.js
const { run } = require("hardhat");

const verifyContract = async (contractAddress) => {
  console.log("Verifying contract...");
  try {
    await run("verify:verify", {
      address: contractAddress,
    });
    console.log("Contract verified!");
  } catch (err) {
    console.log(err);
  }
};

module.exports = { verifyContract };


async function main() {
    const [deployer] = await ethers.getSigners();
    let usdc_address;
    let withdraw_address;

    if (network.name == "matic") {
        usdc_address = "0x2791bca1f2de4661ed88a30c99a7a9449aa84174"; // Replace with the correct USDC address
        withdraw_address = "0x858338Caf83e4cF733210DB9A65151D54Efce2b4";
    } else {
        usdc_address = "0x7De4a91A8D45021EE056840E4337B9c103c54A0b"; // Replace with the correct USDC address
        withdraw_address = "0x750987089d34C529DE03c76eCBC75048f9CF2AF5";
    }

    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    const factory = await ethers.getContractFactory("RumReservationSale");

    await new Promise(resolve => {
        rl.question(`Are you sure you want to deploy? \n\n(y/n)`, async (answer) => {
            if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {
                console.log("deploy starting");

                const contract = await upgrades.deployProxy(factory, [usdc_address, withdraw_address], { initializer: "initialize" });

                console.log("Deployed at: ", contract.address);

                //const proxyAdminAddress = await getAdminAddress(ethers.provider);
                const implementationAddress = await getImplementationAddress(ethers.provider, contract.address);

                //const verifyCmdProxy = `npx hardhat verify --network ${network.name} ${contract.address} --contract "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol:TransparentUpgradeableProxy" --constructor-args "${implementationAddress} ${proxyAdminAddress} \"0x0000000000000000000000000000000000000000000000000000000000000000\""`;
                const verifyCmdImpl = `npx hardhat verify ${implementationAddress} --network ${network.name}  --contract contracts/RumReservationSale.sol:RumReservationSale`;


                console.log("Verify cmd for implementation contract is:");
                console.log(verifyCmdImpl);


            } else {
                console.log("deploy cancelled");
            }
            rl.close();
            resolve();
        });
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
