const BurnSnapShotABI = require('./abis/BurnSnapShot.json');
const ethers = require("ethers").ethers;
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');
dotenv.config();
const provider = new ethers.providers.JsonRpcProvider(process.env.RPC);

const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
const token = new ethers.Contract(process.env.BurnSnapShotContractAddress, BurnSnapShotABI, signer);

export const getListOfHolder = async () => {
    const holders = [];
    try {
        const holdersLength = await token.totalNumberOfHolder();
        for (let i = 0; i < holdersLength; i++) {
            const account = await token.holders(i);
            const balance = await token.balanceOf(account);
            holders.push({ account, balance: balance.toString() });
        }

    } catch (error) {
        console.log(error);
    }
    return holders;
}
getListOfHolder().then(holders => {
    fs.writeFileSync(path.join(__dirname, '/json/holders.json'), JSON.stringify(holders));
});