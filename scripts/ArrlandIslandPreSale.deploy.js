const { ethers } = require("hardhat");
const { exec } = require("child_process");

async function main() {
  const names = [
    "ArrlandSmallIslandPreSaleToken",
    "ArrlandMediumIslandPreSaleToken",
    "ArrlandLargeIslandPreSaleToken",
    "ArrlandHugeIslandPreSaleToken"
  ]

  symbols = [
    "AISPT",
    "AIMPT",
    "AILPT",
    "AIHPT"
    
  ]
  
  burnDelay = 20 * 24 * 60 * 60;
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  console.log("Account balance:", (await deployer.getBalance()).toString());
  console.log("Network:", network.name);
    
  const factory = await ethers.getContractFactory("ArrlandAssetToken");  
  let verifyCmd;
  for (const counter in names) {
    const name = names[counter];
    const symbol = symbols[counter];
    const contract = await factory.deploy(name, symbol, burnDelay);
    console.log("################ START ####################");
    console.log("Deployed at: ", contract.address);
    console.log("name: ", name);
    console.log("Burn burnDelay ", burnDelay);
    verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/ArrlandAssetToken.sol:ArrlandAssetToken ${name} ${symbol} ${burnDelay}`;
    console.log("Verify cmd is:");
    console.log(verifyCmd);
    console.log("################ END ###################");
    exec(verifyCmd, (err, stdout, stderr) => {
        if (err) {
          console.error(`exec error: ${err}`);
          return;
        }
        console.log(`stdout: ${stdout}`);
        console.error(`stderr: ${stderr}`);
      });
  }
}





main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
