const { ethers } = require("hardhat");

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function main() {
    const [deployer] = await ethers.getSigners();
    let rewards_address;
    let safe_address;

    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    const factory = await ethers.getContractFactory("ArrcV1");

    if (network.name == "matic") {
        rewards_address = "0xa4571959859AD23a7c5B80256e3D14Ff3177dFAc"
        safe_address = "0x28cE73C497FB15754148048d061cd108C5100186"
    } else {
        rewards_address = "0x750987089d34C529DE03c76eCBC75048f9CF2AF5"
        safe_address = "0xAAd3Bf33C01cc37A732fbda2B67dfdB8b103c09E"
    }

    await new Promise(resolve => {
        rl.question(`Are you sure you want deploy ? \n rewards_address: ${rewards_address} \n safe_address: ${safe_address}\n\n(y/n)`, async (answer) => {
            if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {
                console.log("deploy starting");

                const contract = await factory.deploy(rewards_address, safe_address);

                console.log("Deployed at: ", contract.address);

                const verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/ArrcV1.sol:ArrcV1 ${rewards_address} ${safe_address}`;
                console.log("Verify cmd is:");
                console.log(verifyCmd);

            } else {
                console.log("deploy cancelled");
            }
            rl.close();
            resolve();
        });
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
