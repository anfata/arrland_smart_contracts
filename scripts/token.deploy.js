const { ethers } = require("hardhat");

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function main() {
    const [deployer] = await ethers.getSigners();
    let pool_creator;
    let owner_transfer;
    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    const factory = await ethers.getContractFactory("Token");

    const total = ethers.utils.parseUnits("1000000000", 18);

    const contract = await factory.deploy("RUM", "RUM", total);

    console.log("Deployed at: ", contract.address);

    const verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/Token.sol:Token RUM RUM ${total}`;
    console.log("Verify cmd is:");
    console.log(verifyCmd);



}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
