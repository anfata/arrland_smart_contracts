const { ethers, upgrades } = require("hardhat");

const fs = require("fs")
const path = require("path")

async function main() {
  // This is just a convenience check
  if (network.name === "hardhat") {
    console.warn(
      "You are trying to deploy a contract to the Hardhat Network, which" +
        "gets automatically created and destroyed every time. Use the Hardhat" +
        " option '--network localhost'"
    );
  }

  // ethers is avaialble in the global scope
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  console.log("Account balance:", (await deployer.getBalance()).toString());


  const ArrLandNFT = await ethers.getContractFactory("ArrLandNFT");
  console.log("Network:", network.name);

  const proxy = await upgrades.deployProxy(ArrLandNFT, ["ipfs://bafybeigxtqimdeqqsorow3tywqgxxvepgfrrgdijvoznqheiiri2cigy6a/", 300, 10000], { initializer: 'initialize' });

  console.log(proxy.deployTransaction.hash)
  console.log("Proxy of ArrLandNFT deployed to:", proxy.address);

  const ArrlandGiveAway = await ethers.getContractFactory("ArrlandGiveAway");

  const giveAway = await ArrlandGiveAway.deploy(proxy.address);

  console.log("ArrlandGiveAway deployed to:", giveAway.address);

  await proxy.setSpawnPirateAllowedCallers(giveAway.address);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


