const { ethers } = require("hardhat");

async function main() {
  const name = "ArrlandFemalePiratePreSaleToken";
  const symbol = "AFPPT";
  
  burnDelay = 16 * 24 * 60 * 60;
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);
    
  const factory = await ethers.getContractFactory("ArrlandAssetToken");  
  const contract = await factory.deploy(name, symbol, burnDelay);

  console.log("Deployed at: ", contract.address);
  console.log("Burn burnDelay ", burnDelay)
  //npx hardhat verify 0x60099bfDE4a7E1F471593571f9c3b32b48281dA7 --network matic --contract contracts/ArrlandAssetToken.sol:ArrlandAssetToken ArrlandFemalePiratePreSaleToken AFPPT 1209600
  console.log("npx hardhat verify  --network "+network.name+ " " + contract.address + " --contract contracts/ArrlandAssetToken.sol:ArrlandAssetToken ArrlandFemalePiratePreSaleToken AFPPT "+burnDelay);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
