const RumReservationSaleABI = require('./abis/RumReservationSale.json');
const ethers = require("ethers").ethers;
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');
dotenv.config();
const provider = new ethers.providers.JsonRpcProvider(process.env.PROD_RPC_MATIC);

const signer = new ethers.Wallet(process.env.PROD_PRIVATE_KEY, provider);
const token = new ethers.Contract("0xcA427165331Ef80376275945F26017d9662822B0", RumReservationSaleABI, signer);

const getListOfHolder = async () => {
    const holders = [];
    try {
        const AllPurchasingAddresses = await token.getAllPurchasingAddresses();

        for (let i = 0; i < AllPurchasingAddresses.length; i++) {
            const account = AllPurchasingAddresses[i];
            console.log(account);
            const PurchasesByUsers = await token.getPurchasesByUser(account);
            for (let j = 0; j < PurchasesByUsers.length; j++) {
                const purchase = PurchasesByUsers[j];
                const rumTokens = (purchase.amountTransfered/10**6)/((purchase.price/10**6)/1000.0);
                holders.push({ name: "public", tokenValue: rumTokens, Address: account });
            }

        }

    } catch (error) {
        console.log(error);
    }
    return holders;
}
const csvFormat = (data) => {
    const csv = data.map(row => Object.values(row).join(';')).join('\n');
    const header = Object.keys(data[0]).join(';') + '\n';
    return header + csv;
}

getListOfHolder().then(holders => {
    const csvData = csvFormat(holders);
    fs.writeFileSync(path.join(__dirname, 'tge-public-0xcA427165331Ef80376275945F26017d9662822B0.csv'), csvData);
});