const { ethers } = require("hardhat");

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function main() {
    const [deployer] = await ethers.getSigners();


    console.log(
        "Deploying the contracts with the account:",
        await deployer.getAddress()
    );

    console.log("Account balance:", (await deployer.getBalance()).toString());
    console.log("Network:", network.name);

    const factory = await ethers.getContractFactory("ARRCTokenClaimer");

    if (network.name == "matic") {
        arrc_address = "0x9fd7833ccE70F62323C0DAd31fDFB12a6a899d73";
    } else {
        arrc_address = "0x16C5Aebc610DADfD8Cae8AFa4487c64ec48FD03b";
    }

    await new Promise(resolve => {
        rl.question(`Are you sure you want deploy ? \n  \n arrc_address: ${arrc_address}\n\n(y/n)`, async (answer) => {
            if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes') {
                console.log("deploy starting");

                const contract = await factory.deploy(arrc_address);

                console.log("Deployed at: ", contract.address);

                const verifyCmd = `npx hardhat verify  --network ${network.name} ${contract.address} --contract contracts/ARRCTokenClaimer.sol:ARRCTokenClaimer ${arrc_address}`;
                console.log("Verify cmd is:");
                console.log(verifyCmd);

            } else {
                console.log("deploy cancelled");
            }
            rl.close();
            resolve();
        });
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
