const { ethers, upgrades } = require('hardhat');

async function main () {

  const currentImp = '0xe804f5A2b14ae03345fffB89bded13A2EF5ceFA7'; // current proxy not imp

  const ArrLandNFTV2 = await ethers.getContractFactory('ArrLandNFTv2');
  console.log('Upgrading ArrLandNFTV2...');
  const instance = await upgrades.upgradeProxy(currentImp, ArrLandNFTV2);
  console.log('ArrLandNFTV2 upgraded');

  console.log(instance.deployTransaction.hash)
  console.log("ArrLandNFTv2 deployed to:", instance.address);

  await instance.setImx("0x5FDCCA53617f4d2b9134B29090C87D01058e27e9");
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });