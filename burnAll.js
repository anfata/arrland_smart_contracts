const BurnSnapShotABI = require('./abis/BurnSnapShot.json');
const ethers = require("ethers").ethers;
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');
const holders = require('./json/holders.json');
dotenv.config();
const provider = new ethers.providers.JsonRpcProvider(process.env.RPC);

const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
const token = new ethers.Contract(process.env.BurnSnapShotContractAddress, BurnSnapShotABI, signer);

(async function () {
    const errorWithBurnTx = [];
    for (let i = 0; i < holders.length; i++) {
        const account = holders[i].account;
        try {
            const tx = await token.burnSingle(account);
            console.log(`${tx.hash}`)
            await tx.wait();
            const balance = await token.balanceOf(account);
            console.log(`${account}: ${balance}`)
        } catch (error) {
            errorWithBurnTx.push(account);
            console.log(error);

        }
    }
    if(errorWithBurnTx.length > 0) {
        console.log("\n\n---------Somthing is wrong with this addresses burn transaction---------------");
        console.log(errorWithBurnTx);
        console.log("\n\n------------------------------------------------------------------------------");
    }
})();

