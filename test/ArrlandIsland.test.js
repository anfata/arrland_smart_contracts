const truffleAssert = require("truffle-assertions");
const { before } = require("lodash");




const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);

describe("ArrlandIsland contract", function () {
    let instance;

    beforeEach(async () => {
        [owner, imx, minter1, minter2, minter3] = await ethers.getSigners();
        ArrlandAsset = await ethers.getContractFactory("ArrlandIsland");
       
        instance = await ArrlandAsset.deploy(imx.address, "http://test/");
        await instance.deployed();
    })

    describe("Test mint", function () {
        it("Should be able to mint", async function () {
            const tokenID = '1';
            const blueprint = '0';
            const blob = toHex(`{${tokenID}}:{${blueprint}}`);
      
            await instance.connect(imx).mintFor(owner.address, 1, blob);
            expect(await instance.balanceOf(owner.address)).to.equal(1); 
            expect(await instance.tokenURI(1)).to.equal("http://test/1"); 
        });     
        
        it("Should be able to mint without blueprint", async function () {
            const tokenID = '1';
            const blueprint = '5';
            const blob = toHex(`{${tokenID}}:{${blueprint}}`);
      
            await instance.connect(imx).mintFor(owner.address, 1, blob);
            expect(await instance.balanceOf(owner.address)).to.equal(1); 
            expect(await instance.tokenURI(1)).to.equal("http://test/1"); 
        }); 

        it("Should be able to set new base url", async function () { 
            await instance.connect(owner).setBaseURI("http://foo/");

            const tokenID = '2';
            const blueprint = '2';
            const blob = toHex(`{${tokenID}}:{${blueprint}}`);
      
            await instance.connect(imx).mintFor(owner.address, 1, blob);
            expect(await instance.tokenURI(2)).to.equal("http://foo/2"); 
        });

        it("Should be able to read total supply", async function () { 
            let tokenID;
            let blueprint;
            let blob;
            
            tokenID = '1';
            blueprint = '0';
            blob = toHex(`{${tokenID}}:{${blueprint}}`);
      
            await instance.connect(imx).mintFor(owner.address, 1, blob);

            tokenID = '5';
            blueprint = '0';
            blob = toHex(`{${tokenID}}:{${blueprint}}`);

            await instance.connect(imx).mintFor(owner.address, 1, blob);
            expect(await instance.totalSupply()).to.equal(2); 
            
        });

        
    })
})

function toHex(str) {
    let result = '';
    for (let i=0; i < str.length; i++) {
      result += str.charCodeAt(i).toString(16);
    }
    return '0x' + result;
  }
  
  function fromHex(str1) {
      let hex = str1.toString().substr(2);
      let str = '';
      for (let n = 0; n < hex.length; n += 2) {
          str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
      }
      return str;
   }