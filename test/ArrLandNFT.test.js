// const { ethers, upgrades } = require("hardhat");
const truffleAssert = require("truffle-assertions");
const { before } = require("lodash");




const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);

describe("ArrLandNFT add many giveaway", function () {
    let instance;
    let giveAway;
    let owner;
    let minter1;
    let minter2;
    let minter3;

    beforeEach(async () => {
        ArrLandNFT = await ethers.getContractFactory("ArrLandNFT");

        ArrlandGiveAway = await ethers.getContractFactory("ArrlandGiveAway");

        [owner, minter1, minter2, minter3] = await ethers.getSigners();
        instance = await upgrades.deployProxy(ArrLandNFT, ["http://test/", 300, 1000]);

        giveAway = await ArrlandGiveAway.deploy(instance.address);
        await giveAway.deployed();

        instance.setSpawnPirateAllowedCallers(giveAway.address);
    })
    describe("Test mint", function () {
        it('test many addreses on whitelist', async () => {
            let m1;
            let m2;
            let m3;
            let m4;
            let m5;
            let m6;
            let m7;
            let m8;
            let m9;
            let m10;

            [m1, m2, m3, m4, m5, m6, m7,m8,m9,m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20] = await ethers.getSigners();
            await giveAway.addWalletsTogiveawaylist([m1.address, m2.address, m3.address, m4.address, m5.address, m6.address, m7.address, m8.address, m9.address, m10.address, m11.address, m12.address, m13.address, m14.address, m15.address, m16.address, m17.address, m18.address, m19.address,m20.address], [2, 2, 5, 2,5, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]);
            expect(await giveAway.giveawaylist(m20.address)).to.be.equal(1)                         
        })
    })
});    


describe("ArrLandNFT contract", function () {
    let instance;
    let giveAway;
    let owner;
    let minter1;
    let minter2;
    let minter3;

    beforeEach(async () => {
        ArrLandNFT = await ethers.getContractFactory("ArrLandNFT");

        ArrlandGiveAway = await ethers.getContractFactory("ArrlandGiveAway");

        [owner, minter1, minter2, minter3] = await ethers.getSigners();
        instance = await upgrades.deployProxy(ArrLandNFT, ["http://test/", 3, 10]);

        giveAway = await ArrlandGiveAway.deploy(instance.address);
        await giveAway.deployed();

        instance.setSpawnPirateAllowedCallers(giveAway.address);


    })
    describe("Test mint", function () {
        it("Should set the right owner", async function () {
            await instance.flipSaleStarted()
            await instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.07", "ether")})
        });
    })
            
    describe("Deployment", function () {
        it("Should set the right owner", async function () {
            expect(await instance.owner()).to.equal(owner.address);        
        });

        it("Should set all initial variables", async function () {
            expect(await instance.publicSalePrice()).to.equal(web3.utils.toWei("0.07", "ether"));
            expect(await instance.preSalePrice()).to.equal(web3.utils.toWei("0.04", "ether"));
            expect(await instance.hasSaleStarted()).to.equal(false);
            expect(await instance.hasPresaleStarted()).to.equal(false);
            expect(await instance.MAX_PRESALE()).to.equal(500);
            expect(await instance.PRE_SALE_MAX()).to.equal(5);
            expect(await instance.PUBLIC_SALE_MAX()).to.equal(10);
            expect(await instance.CURRENT_SALE_PIRATE_TYPE()).to.equal(1);
        });
        it('test correct base url should be set after deploy', async () => {
            await giveAway.sendGiveAway(minter1.address, 1)
            expect(await instance.tokenURI(1)).to.equal("http://test/1");                              
        });
    });

    describe("Owner functions", function () {

        it('test should be able to flipSaleStarted', async () => {
            await instance.flipSaleStarted()
            expect(await instance.hasSaleStarted()).to.equal(true);         
        });
        it('test should be able to flipPreSaleStarted', async () => {
            await instance.flipPreSaleStarted()
            expect(await instance.hasPresaleStarted()).to.equal(true);           
        });
        it('test owner should be able to set base uri for given pirate type', async () => {
            await instance.setBaseURI("http://bar/", 0, 1)
            await giveAway.sendGiveAway(minter1.address, 1)
            expect(await instance.tokenURI(1)).to.equal("http://bar/1");        
        });

        it('test owner should be able to mint tokens', async () => {            
            await instance.flipSaleStarted()
            await giveAway.sendGiveAway(minter1.address, 2)
            
            pirate1 = await instance.arrLanders(0)
            pirate2 = await instance.arrLanders(1)
            expect(await instance.balanceOf(minter1.address)).to.equal(2); 
            pirate1 = await instance.arrLanders(1)
            pirate2 = await instance.arrLanders(2)
            expect(pirate1.pirate_type.toNumber()).to.equal(1);
            expect(pirate2.pirate_type.toNumber()).to.equal(1);
            expect(pirate1.breed_count.toNumber()).to.equal(0);
            expect(pirate2.breed_count.toNumber()).to.equal(0);
            expect(pirate1.generation.toNumber()).to.equal(0);
            expect(pirate2.generation.toNumber()).to.equal(0);
        });
        it('test team should not be able to mint more then reserve of tokens', async () => {
            await giveAway.sendGiveAway(minter1.address, 3)
            await expect(giveAway.sendGiveAway(minter1.address, 1)).to.be.revertedWith('Not reserve left')
        });
        it('test team reserve of tokens should be decrease after mint by owner', async () => {
            pirate_type = await instance.pirate_types(1)
            expect(pirate_type['team_reserve']).to.equal(3);
            await giveAway.sendGiveAway(minter1.address, 1)
            pirate_type = await instance.pirate_types(1)
            expect(pirate_type['team_reserve']).to.equal(2);
        });
        it('test owner should be able to change current sale pirate type', async () => {
            expect(await instance.CURRENT_SALE_PIRATE_TYPE()).to.equal(1);
            await giveAway.sendGiveAway(minter1.address, 1)
            await instance.connect(owner).setPirateSaleType(2,3,10,web3.utils.toWei("0.01", "ether"),web3.utils.toWei("0.02", "ether"));
            await instance.setBaseURI("ipfs://bar/", 0, 2)
            expect(await instance.CURRENT_SALE_PIRATE_TYPE()).to.equal(2)
            await giveAway.sendGiveAway(minter1.address, 1)
            pirate_type1 = await instance.pirate_types(1)
            pirate_type2= await instance.pirate_types(2)
            expect(await instance.tokenURI(2)).to.equal("ipfs://bar/2")
            expect(await instance.tokenURI(1)).to.equal("http://test/1")
            expect(pirate_type1.preSalePrice).to.equal(web3.utils.toWei("0.04", "ether"))
            expect(pirate_type1.publicSalePrice).to.equal(web3.utils.toWei("0.07", "ether"))   
            expect(pirate_type2.preSalePrice).to.equal(web3.utils.toWei("0.01", "ether"))
            expect(pirate_type2.publicSalePrice).to.equal(web3.utils.toWei("0.02", "ether"))      
        });
        it('test owner should be able to mint after sale type change', async () => {
            await instance.setPirateSaleType(2,3,10,web3.utils.toWei("0.01", "ether"),web3.utils.toWei("0.02", "ether"))
            await instance.setBaseURI("ipfs://bar/", 0, 2)
            await giveAway.sendGiveAway(minter1.address, 3)
            expect(await instance.balanceOf(minter1.address)).to.equal(3)
            pirate1 = await instance.arrLanders(1)
            pirate2 = await instance.arrLanders(2)
            pirate3 = await instance.arrLanders(3)
            expect(pirate1.pirate_type).to.equal(2)
            expect(pirate2.pirate_type).to.equal(2)
            expect(pirate3.pirate_type).to.equal(2)
        });
        it('test owner should be able to withdraw funds', async () => {
            await instance.flipSaleStarted()

            await expect(() => instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.07", "ether")})).to.changeEtherBalance(instance, web3.utils.toWei("0.07", "ether"));
            console.log("before")
            console.log(await web3.eth.getBalance(owner.address));

            await expect(() => instance.connect(owner).withdraw()).to.changeEtherBalance(owner, web3.utils.toWei("0.07", "ether"));
            expect(await web3.eth.getBalance(instance.address)).to.be.equal('0')
        });
    });
    describe("Sale flip testcase", function () {
        it('test minter should not be able to flipSaleStarted', async () => {
            await expect(instance.connect(minter1).flipSaleStarted()).to.be.reverted;                         
        });
        it('test minter should not be able to flipPreSaleStarted', async () => {
            await expect(instance.connect(minter1).flipPreSaleStarted()).to.be.reverted;                            
        });
        it('test be able to flipPreSaleStarted', async () => {
            await instance.flipPreSaleStarted();
            expect(await instance.hasPresaleStarted()).to.be.equal(true)
            await instance.flipPreSaleStarted();
            expect(await instance.hasPresaleStarted()).to.be.equal(false)
        });
        it('test be able to flipSaleStarted', async () => {
            await instance.flipSaleStarted();
            expect(await instance.hasSaleStarted()).to.be.equal(true)
            await instance.flipSaleStarted();
            expect(await instance.hasSaleStarted()).to.be.equal(false)
        });
            
    })

    describe("Pre sale testcase", function () {
        beforeEach(async () => {
            await instance.flipPreSaleStarted();
            expect(await instance.hasPresaleStarted()).to.be.equal(true)            
        })

        it('test mint should fail if ether amount does not match tokens count', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await expect(instance.connect(minter1).mint(3, { value: web3.utils.toWei("0.04", "ether")})).to.be.reverted;                
        })

        it('test mint should fail if ether amount is greater then price per token', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await expect(instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.1", "ether")})).to.be.reverted;                
        })

        it('test pre sale mint should fail minting more then allowed max', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await expect(instance.connect(minter1).mint(6, { value: web3.utils.toWei("0.24", "ether")})).to.be.reverted;             
        })

        it('test mint should fail if PreSaleStarted is true, but minter is not on whitelist', async () => {
            await expect(instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})).to.be.revertedWith("The sender isn't eligible for presale");    
        })

        it('test mint should be possible if PreSaleStarted is true, minter is on whitelist', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})
            expect(await instance.balanceOf(minter1.address)).to.equal(1);
        })

        it('test mint should be possible only once on presale', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})
            expect(await instance.balanceOf(minter1.address)).to.equal(1);
            await expect(instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})).to.be.reverted;
        })

        it('test mint should be possible for max 5 tokens', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await instance.connect(minter1).mint(5, { value: web3.utils.toWei("0.2", "ether")})
            expect(await instance.balanceOf(minter1.address)).to.equal(5);
            await expect(instance.connect(minter1).mint(6, { value: web3.utils.toWei("0.24", "ether")})).to.be.reverted;             
        })

        it('test should be able to add many users to whitelist', async () => {
            await instance.addWalletsToWhiteList([minter1.address, minter2.address])
            expect(await instance.whitelist(minter1.address)).to.be.equal(true)
            expect(await instance.whitelist(minter2.address)).to.be.equal(true)
            expect(await instance.whitelist(minter3.address)).to.be.equal(false)
        })

        it('test non owner should not be able to add users to whitelist', async () => {
            await expect(instance.connect(minter1).addWalletsToWhiteList([minter1.address, minter2.address])).to.be.reverted;
            expect(await instance.whitelist(minter1.address)).to.be.equal(false)
            expect(await instance.whitelist(minter2.address)).to.be.equal(false)
            expect(await instance.whitelist(minter3.address)).to.be.equal(false)
        })
    })

    describe("Main sale testcase", function () {

        beforeEach(async () => {
            await instance.flipSaleStarted();
            expect(await instance.hasSaleStarted()).to.be.equal(true)            
        })

        it('test minter should be able to mint tokens in main sale', async () => {
            await expect(() => instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.07", "ether") })).to.changeTokenBalance(instance, minter1, 1);
            pirate1 = await instance.arrLanders(1)
            expect(pirate1.pirate_type).to.be.equal(1)
        });

        it('test mint should fail if ether amount does not match tokens count', async () => {
            await expect(instance.connect(minter1).mint(3, { value: web3.utils.toWei("0.07", "ether")})).to.be.reverted;                
        })

        it('test mint should fail if ether amount is greater then price per token', async () => {
            await instance.addWalletsToWhiteList([minter1.address])
            await expect(instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.1", "ether")})).to.be.reverted;                
        })

        it('test mint should fail if saleStarted is false', async () => {
            await instance.flipSaleStarted()
            await expect(instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.07", "ether")})).to.be.reverted;  
        })

        it('test mint should fail minting more then allowed max', async () => {
            await expect(instance.connect(minter1).mint(11, { value: web3.utils.toWei("0.77", "ether")})).to.be.reverted;             
        })

        it('test mint should fail if tokens count excedes max per type', async () => {
            await instance.connect(minter1).mint(6, { value: web3.utils.toWei("0.42", "ether") })
            await expect(instance.connect(minter1).mint(6, { value: web3.utils.toWei("0.42", "ether")})).to.be.reverted;      
        })

        it('test mint should fail if it would make no token for the team', async () => {
            pirate_type = await instance.pirate_types(1)
            expect(await pirate_type.max_supply).to.be.equal(7)
            expect(pirate_type.supply.toNumber()).to.be.equal(0)
            expect(pirate_type.team_reserve.toNumber()).to.be.equal(3)
            expect(pirate_type.max_supply.toNumber()).to.be.equal(7)
            await expect(instance.connect(minter2).mint(10, { value: web3.utils.toWei("0.42", "ether")})).to.be.revertedWith("Exceeds max_supply");  
        })

        it('test after mint of max supply owner still should mint token for the team', async () => {
            await instance.connect(minter1).mint(7, { value: web3.utils.toWei("0.49", "ether") })
            await giveAway.sendGiveAway(minter2.address, 3)
            pirate_type = await instance.pirate_types(1)
            expect(pirate_type.team_reserve).to.be.equal(0);
            expect(pirate_type.supply).to.be.equal(10);
            expect(await instance.balanceOf(minter1.address)).to.equal(7);
            expect(await instance.balanceOf(minter2.address)).to.equal(3);
        })

        it('test max supply', async () => {
            await giveAway.sendGiveAway(minter1.address, 3)
            await expect(instance.connect(minter1).mint(10, { value: web3.utils.toWei("0.7", "ether") })).to.be.revertedWith("Exceeds max_supply");
            expect(await instance.balanceOf(minter1.address)).to.equal(3);
        })

        it('test mint should be possible for max 10 tokens', async () => {
            instance = await upgrades.deployProxy(ArrLandNFT, ["http://test/", 3, 13]);
            await instance.flipSaleStarted();
            await instance.connect(minter1).mint(10, { value: web3.utils.toWei("0.7", "ether")})
            expect(await instance.balanceOf(minter1.address)).to.equal(10);
            await expect(instance.connect(minter1).mint(11, { value: web3.utils.toWei("0.77", "ether")})).to.be.reverted;             
        })
        
    })

    describe("Minter functions", function () {

        beforeEach(async () => {
            await instance.flipSaleStarted();
            expect(await instance.hasSaleStarted()).to.be.equal(true)            
        })
        it('test minter should not be able to change current sale pirate type', async () => {
            expect(await instance.CURRENT_SALE_PIRATE_TYPE()).to.be.equal(1) 
            expect(instance.connect(minter1).setPirateSaleType(2,3,10,10000000000000000, 20000000000000000)).to.be.reverted;          
            expect(await instance.CURRENT_SALE_PIRATE_TYPE()).to.be.equal(1)
        });
        it('test non owner should not be able to withdraw from contract', async () => {
            await instance.connect(minter1).mint(1, { value: web3.utils.toWei("0.07", "ether") })
            await expect(instance.connect(minter1).withdraw()).to.be.reverted;
        });
    });

    describe("Giveaway reserve and claim", function () {
        it('test reserve all give aways tokens', async () => {
            await giveAway.addWalletsTogiveawaylist([minter2.address, minter1.address], [2, 1]);
            expect(await giveAway.giveawaylist(minter1.address)).to.be.equal(1)
            expect(await giveAway.giveawaylist(minter2.address)).to.be.equal(2)
        });
        it('test only owner should be able to add to give away list', async () => {
            expect(giveAway.connect(minter1).addWalletsTogiveawaylist([minter1.address, minter2.address, minter3.address], [1, 2, 1])).to.be.reverted; 
        });
        it('test should be able to claim give away', async () => {
            await giveAway.addWalletsTogiveawaylist([minter1.address], [1], []);
            await giveAway.connect(minter1).claimGiveAway();
            expect(await instance.balanceOf(minter1.address)).to.equal(1); 
        });
        it('test should be able to claim only once', async () => {
            await giveAway.addWalletsTogiveawaylist([minter1.address, minter3.address], [1,1]);
            await giveAway.connect(minter1).claimGiveAway();
            expect(giveAway.connect(minter1).claimGiveAway()).to.be.revertedWith("Give away alredy claimed");  
        });


    })
});
