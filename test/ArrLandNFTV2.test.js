// const { ethers, upgrades } = require("hardhat");
const truffleAssert = require("truffle-assertions");
const { before } = require("lodash");




const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);


describe("ArrLandNFT upgrade", function () {
    let instance;
    let instanceV2;
    let giveAway;
    let owner;
    let minter1;
    let minter2;
    let minter3;

    beforeEach(async () => {
        ArrLandNFT = await ethers.getContractFactory("ArrLandNFT");

        ArrlandGiveAway = await ethers.getContractFactory("ArrlandGiveAway");

        [owner, imx, minter1, minter2, minter3] = await ethers.getSigners();
        instance = await upgrades.deployProxy(ArrLandNFT, ["http://test/", 300, 10000]);

        giveAway = await ArrlandGiveAway.deploy(instance.address);
        await giveAway.deployed();

        instance.setSpawnPirateAllowedCallers(giveAway.address);

        const range = (start, end, length = end - start + 1) => Array.from({ length }, (_, i) => start + i);

        const tokens = range(1,106);

        for (let i = 0; i < tokens.length; i++) {        
          await giveAway.connect(owner).sendGiveAway(minter1.address, 1)          
        };

        pirate_type = await instance.pirate_types(1)
        expect(pirate_type.team_reserve).to.be.equal(194);
        expect(pirate_type.supply).to.be.equal(106);
        expect(pirate_type.max_supply).to.be.equal(9700);

        const ArrLandNFTV2 = await ethers.getContractFactory('ArrLandNFTv2');
        console.log('Upgrading ArrLandNFTV2...');
        instanceV2 = await upgrades.upgradeProxy(instance.address, ArrLandNFTV2);
        await instanceV2.setImx(imx.address);
        console.log('Box upgraded');    


    })

    it("Old mint should bee reverted", async function () {
      await expect(instanceV2.connect(minter1).mint(2)).to.be.revertedWith("mint on L2"); 
    })

    it("Should be able to mint successfully with blueprint pirates type 2", async function () {
      await instanceV2.setPirateSaleType(2, 0, 0, web3.utils.toWei("0.04", "ether"), web3.utils.toWei("0.07", "ether"));

      const tokenID = '10001';
      const blueprint = '2';
      const blob = toHex(`{${tokenID}}:{${blueprint}}`);

      await instanceV2.connect(imx).mintFor(owner.address, 1, blob);

      pirate1 = await instanceV2.arrLanders(10001)
      expect(await instanceV2.balanceOf(owner.address)).to.equal(1); 
      expect(pirate1.pirate_type.toNumber()).to.equal(2);
      expect(pirate1.generation.toNumber()).to.equal(0);
      expect(await instanceV2.connect(owner).totalSupply()).to.equal(107);

    });

    it("Should be able to mint successfully with blueprint", async function () {

        pirate_type = await instance.pirate_types(1)
        expect(pirate_type.team_reserve).to.be.equal(194);
        expect(pirate_type.supply).to.be.equal(106);
        expect(pirate_type.max_supply).to.be.equal(9700);

        expect(await instanceV2.imx()).to.be.equal(imx.address);
        const tokenID = '107';
        const blueprint = '1';
        const blob = toHex(`{${tokenID}}:{${blueprint}}`);
      
        await instanceV2.connect(imx).mintFor(owner.address, 1, blob);

        pirate1 = await instanceV2.arrLanders(1)
        expect(await instanceV2.balanceOf(owner.address)).to.equal(1); 
        expect(pirate1.pirate_type.toNumber()).to.equal(1);
        expect(pirate1.generation.toNumber()).to.equal(0);
        expect(await instanceV2.connect(owner).totalSupply()).to.equal(107);


    });
    it("Should not be able to mint above max supply", async function () {
        expect(await instanceV2.imx()).to.be.equal(imx.address);

        const range = (start, end, length = end - start + 1) => Array.from({ length }, (_, i) => start + i);

        const tokens = range(107,10000);

        for (let i = 0; i < tokens.length; i++) {
          console.log(tokens[i]);
          await instanceV2.connect(imx).mintFor(owner.address, 1, toHex("{"+tokens[i]+"}:{1}"));

        };

        expect(await instanceV2.connect(owner).totalSupply()).to.equal(10000);

        const tokenID = '10001';
        const blueprint = '1';
        const blob = toHex(`{${tokenID}}:{${blueprint}}`);
        await expect(instanceV2.connect(imx).mintFor(owner.address, 1, blob)).to.be.revertedWith("max supply");
    });

    it("Should not be able to mint above max supply type", async function () {
        expect(await instanceV2.imx()).to.be.equal(imx.address);

        const range = (start, end, length = end - start + 1) => Array.from({ length }, (_, i) => start + i);

        const tokens = range(10001,15000);

        for (let i = 0; i < tokens.length; i++) {
          console.log(tokens[i]);
          await instanceV2.connect(imx).mintFor(owner.address, 1, toHex("{"+tokens[i]+"}:{2}"));

        };

        expect(await instanceV2.connect(owner).totalSupply()).to.equal(5106);

        const tokenID = '15001';
        const blueprint = '2';
        const blob = toHex(`{${tokenID}}:{${blueprint}}`);
        await expect(instanceV2.connect(imx).mintFor(owner.address, 1, blob)).to.be.revertedWith("max supply");
    });
});    

  function toHex(str) {
  let result = '';
  for (let i=0; i < str.length; i++) {
    result += str.charCodeAt(i).toString(16);
  }
  return '0x' + result;
}

function fromHex(str1) {
	let hex = str1.toString().substr(2);
	let str = '';
	for (let n = 0; n < hex.length; n += 2) {
		str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
	}
	return str;
 }