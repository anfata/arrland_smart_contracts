// const { ethers, upgrades } = require("hardhat");
const truffleAssert = require("truffle-assertions");
const { before } = require("lodash");




const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);


describe("BurnSnapshot", function () {
    let totalSupply = 3758305;
    let pool_creator;
    let owner_transfer;
    let users = [];
    let token;
    let balances = [];
    beforeEach(async () => {
        [owner, bidder1, bidder2, bidder3, bidder4] = await ethers.getSigners();
        users = await ethers.getSigners();
        for (let i = 0; i < users.length; i++)
            balances.push("0");

        const BurnSnapShot = await ethers.getContractFactory("BurnSnapShot");
        pool_creator = owner.address
        owner_transfer = owner.address;
        token = await BurnSnapShot.deploy(pool_creator, owner_transfer);
    })

    it("OwnerShip", async function () {
        const currentOwner = await token.owner();
        expect(currentOwner).to.be.eq(owner_transfer);
    });

    it("Transfer to different account", async () => {
        for (let i = 1; i < users.length; i++) {
            const bal = ethers.utils.parseEther((0.01 * totalSupply).toString());
            await token.transfer(users[i].address, bal);
        }
        for (let i = 1; i < users.length; i++) {
            const balance = await token.balanceOf(users[i].address);
            balances[i] = ethers.utils.parseEther((0.01 * totalSupply).toString())
            expect(balance.toString()).to.be.eq(balances[i]);
        }
        balances[0] = await token.balanceOf(users[0].address);

    });

    it("TransferFrom owner to user 3", async () => {
        await token.transfer(users[1].address, "1000000");
        await token.connect(users[1]).approve(users[2].address, "1000000");
        const balanceBefore = await token.balanceOf(owner.address);
        await token.connect(users[2]).transferFrom(users[1].address, owner.address, "1000000");
        const balanceAfter = await token.balanceOf(owner.address);
        expect(balanceAfter.sub(balanceBefore).toString()).to.be.eq("1000000");
    })

    it("Transfer block", async () => {
        await token.setTransferBlock(true);
        await expect(token.transfer(users[0].address, "1000000000"))
            .to.be.revertedWith("Transfer is Blocked");
    });

    it("TransferFrom owner to user 3 should Revert After Transfer block.", async () => {
        await token.setTransferBlock(true);
        await token.approve(users[0].address, "1000000");
        await expect(token.connect(users[0]).transferFrom(owner.address, users[1].address, "1000000")).to.be.revertedWith("Transfer is Blocked");
    })

    // it("Burn all", async ()=>{
    //     const balances = ["0"];
    //     for (let i = 1; i < users.length; i++) {
    //         const bal = ethers.utils.parseEther((0.01 * totalSupply).toString());
    //         await token.transfer(users[i].address, bal);
    //         balances.push("0");
    //     }
    //     for (let i = 1; i < users.length; i++) {
    //         const balance = await token.balanceOf(users[i].address);
    //         balances[i] = ethers.utils.parseEther((0.01 * totalSupply).toString())
    //         console.log(`${users[i].address}: ${balance.toString()}`);
    //         expect(balance.toString()).to.be.eq(balances[i]);
    //     } 
    //     const holdersLength = await token.totalNumberOfHolder();
    //     const holders = [];
    //     for(let i = 0; i < holdersLength; i++) {
    //         holders.push(await token.holders(i));
    //     }
    //     for(let i = 0; i < holders.length; i++) {
    //         await token.burnSingle(holders[i]);
    //         const balance = await token.balanceOf(holders[i]);
    //         console.log(`${holders[i]}: ${balance.toString()}`);
    //         expect(balance.toString()).to.be.eq("0");
    //     }
    // });

});    
