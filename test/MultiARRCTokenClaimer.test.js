const { expect } = require("chai");
const { BigNumber } = ethers;
const { MerkleTree } = require("merkletreejs")
const keccak256 = require("ethereumjs-util").keccak256;

const generateProofs = (users) => {
    let leaves = users.map((user) => keccak256(ethers.utils.solidityPack(["uint256", "address", "uint256"], [user.distributionId, user.address, user.amount])));
    let merkleTree = new MerkleTree(leaves, keccak256, {sortPairs: true});
    let rootHash = merkleTree.getRoot().toString('hex')
    const proofs = users.map(user => {
        const leaf = keccak256(ethers.utils.solidityPack(["uint256", "address", "uint256"], [user.distributionId, user.address, user.amount]));
        return {
            address: user.address,
            distributionId: user.distributionId,
            amount: user.amount,
            proof: merkleTree.getProof(leaf).map(ele=>"0x"+ele.data.toString('hex'))
        }
    });
    return {
        rootHash: `0x${rootHash}`,
        proofs
    }
}


describe("MultiARRCTokenClaimer", function () {
    let owner, admin, user1, user2, arrcToken, arrcTokenClaimer;

    beforeEach(async function () {
        [owner, admin, user1, user2, reward, safe] = await ethers.getSigners();

        // Deploy the ARRC token
        const Token = await ethers.getContractFactory("ArrcV1");
        arrcToken = await Token.deploy(owner.address, owner.address);

        // Deploy the ARRCTokenClaimer contract
        const MultiARRCTokenClaimer = await ethers.getContractFactory("MultiARRCTokenClaimer");
        arrcTokenClaimer = await MultiARRCTokenClaimer.deploy(arrcToken.address);

        // Set the admin role
        await arrcTokenClaimer.setAdmin(admin.address);
    });

    it("should set the admin correctly", async function () {
        const adminAddress = await arrcTokenClaimer.admin();
        expect(await arrcTokenClaimer.admin()).to.equal(adminAddress);
    });

    it("should not allow non-admins to add a distribution", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: 100}];
        const proofs = generateProofs(users);
        await expect(
            arrcTokenClaimer.connect(user1).addDistribution(1, proofs.rootHash, 100)
        ).to.be.revertedWith("Caller is not admin or owner");
    });

    it("Should allow admin to add distribution", async function () {
        const users = [
            {distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("10")},
            {distributionId: 1, address: user2.address, amount: ethers.utils.parseEther("20")}
        ];
        const { rootHash } = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, rootHash, ethers.utils.parseEther("30"));
        expect(await arrcTokenClaimer.totalAmounts(1)).to.equal(ethers.utils.parseEther("30"));
    });

    it("should not allow a claim if the amount is more than the total distribution amount", async function () {
        const users = [
            {distributionId: 1, address: user1.address, amount: 101},
            {distributionId: 1, address: user1.address, amount: 100}
            ];
        const proofs = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, 100);
        await expect(
            arrcTokenClaimer.connect(user1).claim(1, 101, proofs.proofs[0].proof)
        ).to.be.revertedWith("Claim exceeds total distribution amount");
    });

    it("Should allow users to claim tokens", async function () {
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("30", 18));

        const users = [
            {distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("10")},
            {distributionId: 1, address: user2.address, amount: ethers.utils.parseEther("20")}
        ];
        const { rootHash, proofs } = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, rootHash, ethers.utils.parseEther("30"));
        await arrcTokenClaimer.connect(user1).claim(1, proofs[0].amount, proofs[0].proof);
        expect(await arrcToken.balanceOf(user1.address)).to.equal(proofs[0].amount);
    });

    it("should allow a valid claim", async function () {
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("100", 18));
        const users = [{distributionId: 1, address: user1.address, amount: 100}];
        const proofs = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, 100);
        await arrcToken.connect(owner).transfer(arrcTokenClaimer.address, 100);
        await arrcTokenClaimer.connect(user1).claim(1, 100, proofs.proofs[0].proof);
        expect(await arrcToken.balanceOf(user1.address)).to.equal("100");
    });

    it("Should not allow users to claim more tokens than allowed", async function () {
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("30", 18));
        const users = [
            {distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("10")},
            {distributionId: 1, address: user2.address, amount: ethers.utils.parseEther("20")}
        ];
        const { rootHash, proofs } = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, rootHash, ethers.utils.parseEther("30"));
        await expect(arrcTokenClaimer.connect(user1).claim(1, ethers.utils.parseEther("15"), proofs[0].proof)).to.be.revertedWith("Invalid proof");
        await expect(arrcTokenClaimer.connect(user1).claim(1, ethers.utils.parseEther("5"), proofs[0].proof)).to.be.revertedWith("Invalid proof");
    });

    it("should not allow a claim if it's already claimed", async function () {
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("200", 18));
        const users = [
            {distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)},
            {distributionId: 1, address: user2.address, amount: ethers.utils.parseEther("100", 18)}
        ];
        const proofs = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await arrcTokenClaimer.connect(user1).claim(users[0].distributionId, users[0].amount, proofs.proofs[0].proof);
        expect(await arrcToken.balanceOf(user1.address)).to.equal(ethers.utils.parseEther("100", 18));
        await expect(arrcTokenClaimer.connect(user1).claim(users[0].distributionId, users[0].amount, proofs.proofs[0].proof)).to.be.revertedWith("Already claimed");

    });

    it("should not allow a claim if the contract's balance is less than the claim amount", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("200", 18)}];
        const proofs = generateProofs(users);
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("100", 18)); //less balance than claim amount
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await expect(arrcTokenClaimer.connect(user1).claim(1, ethers.utils.parseEther("200", 18), proofs.proofs[0].proof)).to.be.revertedWith("Claim exceeds contract's ARRC token balance");
    });

    it("should not allow a claim with an invalid distribution id", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)}];
        const proofs = generateProofs(users);
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("200", 18));
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await expect(arrcTokenClaimer.connect(user1).claim(2, ethers.utils.parseEther("100", 18), proofs.proofs[0].proof)).to.be.revertedWith("Invalid distributionId");
    });

    it("should not allow a claim with an invalid merkle proof", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)}];
        const proofs = generateProofs(users);
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("200", 18));
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        const invalidProof = ["0x0000000000000000000000000000000000000000000000000000000000000000"];
        await expect(arrcTokenClaimer.connect(user1).claim(1, ethers.utils.parseEther("100", 18), invalidProof)).to.be.revertedWith("Invalid proof");
    });

    it("should allow a claim when contract's balance is exactly the claim amount", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)}];
        const proofs = generateProofs(users);
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("100", 18)); //exact balance as claim amount
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await arrcTokenClaimer.connect(user1).claim(1, ethers.utils.parseEther("100", 18), proofs.proofs[0].proof);
        expect(await arrcToken.balanceOf(user1.address)).to.equal(ethers.utils.parseEther("100", 18));
    });

    it("should allow admin to remove a distribution", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)}];
        const proofs = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await arrcTokenClaimer.connect(admin).removeDistribution(1);
        expect(await arrcTokenClaimer.merkleRoots(1)).to.equal("0x0000000000000000000000000000000000000000000000000000000000000000");
        expect(await arrcTokenClaimer.totalAmounts(1)).to.equal(0);
    });

    it("should not allow non-admin to remove a distribution", async function () {
        const users = [{distributionId: 1, address: user1.address, amount: ethers.utils.parseEther("100", 18)}];
        const proofs = generateProofs(users);
        await arrcTokenClaimer.connect(admin).addDistribution(1, proofs.rootHash, ethers.utils.parseEther("200", 18));
        await expect(arrcTokenClaimer.connect(user1).removeDistribution(1)).to.be.revertedWith("Caller is not admin or owner");
    });

    it("should revert when trying to remove a non-existent distribution", async function () {
        await expect(arrcTokenClaimer.connect(admin).removeDistribution(1)).to.be.revertedWith("Distribution not found");
    });

    it("owner should be able to withdraw unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("1000", 18));

        // Get the owner's initial balance
        const initialOwnerBalance = await arrcToken.balanceOf(owner.address);

        // Owner withdraws unclaimed tokens
        const unclaimedAmount = ethers.utils.parseEther("500", 18);
        await arrcTokenClaimer.connect(owner).withdrawUnclaimedTokens(unclaimedAmount);

        // Check that the owner received the unclaimed tokens
        const ownerBalance = await arrcToken.balanceOf(owner.address);
        expect(ownerBalance).to.equal(initialOwnerBalance.add(unclaimedAmount));
    });

    it("should revert if owner tries to withdraw more than unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("1000", 18));

        // Owner tries to withdraw an amount greater than unclaimed tokens
        const excessiveWithdrawalAmount = ethers.utils.parseEther("1500", 18);
        await expect(arrcTokenClaimer.connect(owner).withdrawUnclaimedTokens(excessiveWithdrawalAmount)).to.be.revertedWith("Withdrawal amount exceeds unclaimed tokens");
    });
    it("should revert if a non-owner tries to withdraw unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, ethers.utils.parseEther("1000", 18));

        // Non-owner (e.g., user1) tries to withdraw unclaimed tokens
        const unclaimedAmount = ethers.utils.parseEther("500", 18);
        await expect(arrcTokenClaimer.connect(user1).withdrawUnclaimedTokens(unclaimedAmount)).to.be.revertedWith("Ownable: caller is not the owner");
    });





});