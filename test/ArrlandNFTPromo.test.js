// const { ethers, upgrades } = require("hardhat");
const truffleAssert = require("truffle-assertions");
const { before } = require("lodash");




const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);

describe("ArrlandNFTPromo contract", function () {
    let instance;
    let promo;
    let owner;
    let minter1;
    let minter2;
    let minter3;

    beforeEach(async () => {
        ArrLandNFT = await ethers.getContractFactory("ArrLandNFT");

        ArrlandNFTPromo = await ethers.getContractFactory("ArrlandNFTPromo");

        [owner, minter1, minter2, minter3] = await ethers.getSigners();
        instance = await upgrades.deployProxy(ArrLandNFT, ["http://test/", 300, 10000]);

        //promo = await ArrlandNFTPromo.deploy(instance.address, [web3.utils.toWei("0.04", "ether"), web3.utils.toWei("0.045", "ether"), web3.utils.toWei("0.05", "ether")], [100, 300, 1500], [5,4,4]);
        promo = await ArrlandNFTPromo.deploy(instance.address, [50,50,50]);
        await promo.deployed();

        instance.setSpawnPirateAllowedCallers(promo.address);

        await instance.flipSaleStarted();

        await instance.connect(minter1).mint(10, { value: web3.utils.toWei("0.7", "ether")})
        await instance.connect(minter1).mint(10, { value: web3.utils.toWei("0.7", "ether")})
        await instance.connect(minter1).mint(6, { value: web3.utils.toWei("0.42", "ether")})

    })

    describe("Test mint", function () {
        it("Should be able to mint in promo price lv1 1", async function () {
            expect(await instance.balanceOf(minter1.address)).to.equal(26);      
            await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})   
            expect(await instance.balanceOf(minter1.address)).to.equal(27);         
        });

        // it('test owner should be able to withdraw funds', async () => {
        //     await expect(() => promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})).to.changeEtherBalance(promo, web3.utils.toWei("0.04", "ether"));
        //     await expect(() => promo.connect(owner).withdraw()).to.changeEtherBalance(owner, web3.utils.toWei("0.04", "ether"));
        //     expect(await web3.eth.getBalance(promo.address)).to.be.equal('0')
        // });
        // it("Price level 2 should mint when buying more", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("1.96", "ether")})
        //     await promo.connect(minter1).mint(24, { value: web3.utils.toWei("0.96", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(199)
        //     await promo.connect(minter1).mint(5, { value: web3.utils.toWei("0.2", "ether")})
        //     //await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
            
        //     //await expect(promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})  ).to.be.reverted;        
        // });
        // it("ID > 200", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("1.96", "ether")})
        //     await promo.connect(minter1).mint(24, { value: web3.utils.toWei("0.96", "ether")})
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(200)
        //     await expect(promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})  ).to.be.reverted;      


                    
        // });

        // it("ID > 400", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("1.96", "ether")})
        //     await promo.connect(minter1).mint(24, { value: web3.utils.toWei("0.96", "ether")})
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(200)
            
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("2.205", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(399)
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.045", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(400)
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.05", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(401)

                    
        // });

        // it("should be able to set levels", async function () {
        //     await promo.connect(owner).setParams([3,2,1]);
        //     let max_mint1;
        //     let max_mint2;
        //     let max_mint3;
        //     max_mint1 = (await promo.connect(owner).mintLvlmax(0)).toNumber();
        //     max_mint2 = (await promo.connect(owner).mintLvlmax(1)).toNumber();
        //     max_mint3 = (await promo.connect(owner).mintLvlmax(2)).toNumber();
        //     await expect(max_mint1).to.be.equal(3);
        //     await expect(max_mint2).to.be.equal(2);
        //     await expect(max_mint3).to.be.equal(1);
        // });

        // it("max mint should be reverted", async function () {
        //     await expect(promo.connect(minter1).mint(51, { value: web3.utils.toWei("2.55", "ether")})  ).to.be.reverted;   
        // });

        // it("promo ends on 2474", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("1.96", "ether")})
        //     await promo.connect(minter1).mint(24, { value: web3.utils.toWei("0.96", "ether")})
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.04", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(200)
            
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("2.205", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(399)
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.045", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(400)
            
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(1400)

        // });
            

        // it("token 300 should be for 0.045", async function () {
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.4", "ether")})

        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})

        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(10, { value: web3.utils.toWei("0.45", "ether")})
        //     await promo.connect(minter1).mint(9, { value: web3.utils.toWei("0.405", "ether")})

        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.045", "ether")})
        //     await expect(promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.045", "ether")})  ).to.be.reverted;         
        // });

        // it("token above 300 should be for 0.05", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.05", "ether")})
        // });

        // it("should not be able to mint more then 2500", async function () {
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.25", "ether")})


        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
            
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})
        //     await promo.connect(minter1).mint(50, { value: web3.utils.toWei("2.5", "ether")})

        //     await promo.connect(minter1).mint(49, { value: web3.utils.toWei("2.45", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(2499)
        //     await promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.05", "ether")})
        //     expect(await promo.CURRENT_TOKEN_ID()).to.be.equal(2500)

            //await expect(promo.connect(minter1).mint(1, { value: web3.utils.toWei("0.05", "ether")}) ).to.be.reverted;         

            
        //});


            


    })
})