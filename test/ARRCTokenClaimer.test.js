const { expect } = require("chai");
const { ethers } = require("hardhat");
const { utils } = require("ethers");
const { parseUnits } = utils;

const MerkleTree = require("merkletreejs").MerkleTree;
const keccak256 = require("ethereumjs-util").keccak256;

function createMerkleTree(users) {
  console.log(typeof ethers.utils.solidityPack(["address", "uint256", "uint256"], [users[0].address, users[0].amount, users[0].index]));
  const leaves = users.map(({ address, amount, index }) => keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [address, amount, index])));
  const merkleTree = new MerkleTree(leaves, keccak256, { sortPairs: true });
  return merkleTree;
}

describe("ARRCTokenClaimer", function () {
    let owner, admin, user1, user2, arrcToken, arrcTokenClaimer;

    beforeEach(async function () {
        [owner, admin, user1, user2, reward, safe] = await ethers.getSigners();

        // Deploy the ARRC token
        const Token = await ethers.getContractFactory("ArrcV1");
        arrcToken = await Token.deploy(owner.address, owner.address);

        // Deploy the ARRCTokenClaimer contract
        const ARRCTokenClaimer = await ethers.getContractFactory("ARRCTokenClaimer");
        arrcTokenClaimer = await ARRCTokenClaimer.deploy(arrcToken.address);

        // Set the admin role
        await arrcTokenClaimer.setAdmin(admin.address);
    });

    it("should set correct admin and owner", async function () {
        expect(await arrcTokenClaimer.admin()).to.equal(admin.address);
        expect(await arrcTokenClaimer.owner()).to.equal(owner.address);
    });

    it("admin should be able to set Merkle root", async function () {
        const newMerkleRoot = utils.keccak256("0x1234");
        await arrcTokenClaimer.connect(admin).setMerkleRoot(newMerkleRoot);
        expect(await arrcTokenClaimer.merkleRoot()).to.equal(newMerkleRoot);
    });

    it("non-admin should not be able to set Merkle root", async function () {
        const newMerkleRoot = utils.keccak256("0x1234");
        await expect(arrcTokenClaimer.connect(user1).setMerkleRoot(newMerkleRoot)).to.be.revertedWith("Caller is not admin");
    });

    it("user should be able to claim tokens with valid Merkle proof", async function () {
        const users = [
            { address: user1.address, amount: parseUnits("100", 18), index: 0 },
            { address: user2.address, amount: parseUnits("200", 18), index: 1 },
        ];
        const merkleTree = createMerkleTree(users);
        const merkleRoot = merkleTree.getHexRoot();
        const user1Proof = merkleTree.getHexProof(keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [user1.address, users[0].amount, users[0].index])));

        // Set Merkle root in the contract
        await arrcTokenClaimer.connect(admin).setMerkleRoot(merkleRoot);

        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // User1 claims tokens with a valid Merkle proof
        await arrcTokenClaimer.connect(user1).claimTokens(users[0].amount, users[0].index, user1Proof);
    });

    it("user should not be able to claim tokens with invalid Merkle proof", async function () {
        // Prepare Merkle tree and proof
        console.log(parseUnits("0.02", 18).toString())
        const users = [
          { address: user1.address, amount: parseUnits("100", 18), index: 0 },
          { address: user2.address, amount: parseUnits("200", 18), index: 1 },
        ];
        const merkleTree = createMerkleTree(users);
        const merkleRoot = merkleTree.getHexRoot();
        const user1Proof = merkleTree.getHexProof(keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [user1.address, users[0].amount, users[0].index])));
        const user2Proof = merkleTree.getHexProof(keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [user2.address, users[1].amount, users[1].index])));

        // Set Merkle root in the contract
        await arrcTokenClaimer.connect(admin).setMerkleRoot(merkleRoot);

        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // User1 claims tokens with an invalid Merkle proof (e.g., using user2's proof)
        await expect(arrcTokenClaimer.connect(user1).claimTokens(users[1].amount, users[1].index, user2Proof)).to.be.revertedWith("Invalid Merkle proof");
    });

    it("user should not be able to claim tokens twice", async function () {
        // Prepare Merkle tree and proof
        const users = [
          { address: user1.address, amount: parseUnits("100", 18), index: 0 },
          { address: user2.address, amount: parseUnits("200", 18), index: 1 },
        ];
        const merkleTree = createMerkleTree(users);
        const merkleRoot = merkleTree.getHexRoot();
        const user1Proof = merkleTree.getHexProof(keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [user1.address, users[0].amount, users[0].index])));

        // Set Merkle root in the contract
        await arrcTokenClaimer.connect(admin).setMerkleRoot(merkleRoot);

        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // User1 claims tokens with a valid Merkle proof
        await arrcTokenClaimer.connect(user1).claimTokens(users[0].amount, users[0].index, user1Proof);

        // User1 tries to claim tokens again with the same proof
        await expect(arrcTokenClaimer.connect(user1).claimTokens(users[0].amount, users[0].index, user1Proof)).to.be.revertedWith("Tokens already claimed");
    });
    it("owner should be able to withdraw unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // Get the owner's initial balance
        const initialOwnerBalance = await arrcToken.balanceOf(owner.address);

        // Owner withdraws unclaimed tokens
        const unclaimedAmount = parseUnits("500", 18);
        await arrcTokenClaimer.connect(owner).withdrawUnclaimedTokens(unclaimedAmount);

        // Check that the owner received the unclaimed tokens
        const ownerBalance = await arrcToken.balanceOf(owner.address);
        expect(ownerBalance).to.equal(initialOwnerBalance.add(unclaimedAmount));
    });
    it("should revert if there are insufficient tokens in the contract to claim", async function () {
        // Prepare Merkle tree and proof
        const users = [
            { address: user1.address, amount: parseUnits("100", 18), index: 0 },
            { address: user2.address, amount: parseUnits("200", 18), index: 1 },
        ];
        const merkleTree = createMerkleTree(users);
        const merkleRoot = merkleTree.getHexRoot();
        const user1Proof = merkleTree.getHexProof(keccak256(ethers.utils.solidityPack(["address", "uint256", "uint256"], [user1.address, users[0].amount, users[0].index])));

        // Set Merkle root in the contract
        await arrcTokenClaimer.connect(admin).setMerkleRoot(merkleRoot);

        // Transfer only 50 tokens to the contract, less than user1's claim amount
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("50", 18));

        // User1 tries to claim tokens with a valid Merkle proof but insufficient tokens in the contract
        await expect(arrcTokenClaimer.connect(user1).claimTokens(users[0].amount, users[0].index, user1Proof)).to.be.revertedWith("Not enough tokens in the contract");
    });
    it("should revert if owner tries to withdraw more than unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // Owner tries to withdraw an amount greater than unclaimed tokens
        const excessiveWithdrawalAmount = parseUnits("1500", 18);
        await expect(arrcTokenClaimer.connect(owner).withdrawUnclaimedTokens(excessiveWithdrawalAmount)).to.be.revertedWith("Withdrawal amount exceeds unclaimed tokens");
    });
    it("should revert if a non-owner tries to withdraw unclaimed tokens", async function () {
        // Transfer tokens to the contract
        await arrcToken.transfer(arrcTokenClaimer.address, parseUnits("1000", 18));

        // Non-owner (e.g., user1) tries to withdraw unclaimed tokens
        const unclaimedAmount = parseUnits("500", 18);
        await expect(arrcTokenClaimer.connect(user1).withdrawUnclaimedTokens(unclaimedAmount)).to.be.revertedWith("Ownable: caller is not the owner");
    });






});
