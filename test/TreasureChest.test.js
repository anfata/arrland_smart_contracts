const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");
const { generateProof } = require("./merkleUtils");
const { expectEvent } = require('truffle-assertions');

use(solidity);

describe("TreasureChest contract", function () {
    let instance;
    let arrcToken;
    let owner, whitelistedUser, nonWhitelistedUser, withdrawAddress;
    let merkleInfo;
    let merkleProof;
    let merkleProofAddreses;
    const arbitraryRoot = "0x0000000000000000000000000000000000000000000000000000000000000000";
    const emptyProof = [];
    const MAX_LEVEL = 8;
    let pricesInWei;
    let whitelistedUsers;
    let baseURIpath;

    beforeEach(async () => {
        [owner, whitelistedUser, nonWhitelistedUser, whitelistedUser2, ...whitelistedUsers] = await ethers.getSigners();

        const Token = await ethers.getContractFactory("ArrcV1");
        arrcToken = await Token.deploy(owner.address, owner.address);

        // // Deploy the ARRCTokenClaimer contract
        const TreasureChest = await ethers.getContractFactory("TreasureChest");

        const prices = [6, 17, 35, 60, 90, 120, 155];
        pricesInWei = prices.map((price) => ethers.utils.parseEther(price.toString()));
        baseURIpath = "https://test/"; // replace with your base URI

        instance = await TreasureChest.deploy(baseURIpath);
        await instance.initializeLevelUpPrice(pricesInWei);

        merkleProofAddreses = [whitelistedUser, whitelistedUser2];
        merkleInfo = generateProof(merkleProofAddreses.map(address => address.address));

        //updateContractSettings(bool publicMintingEnabled, bool whitelistMintingEnabled, bool openEnabled);

        //await instance.connect(owner).updateContractSettings(true, false, false);
        await instance.connect(owner).updateMerkleRoot(merkleInfo.rootHash);

    });

    describe('setAdmin', () => {
        it('should allow the owner to set a new admin', async () => {
          const newAdmin = whitelistedUser;

          await instance.connect(owner).setAdmin(newAdmin.address);

          // Verify the updated admin value
          const updatedAdmin = await instance.admin();
          expect(updatedAdmin).to.equal(newAdmin.address);
        });

        it('should revert when a non-owner tries to set a new admin', async () => {
          const newAdmin = whitelistedUser;

          // Call setAdmin from a non-owner account and expect it to revert
          await expect(
            instance.connect(nonWhitelistedUser).setAdmin(newAdmin.address)
          ).to.be.revertedWith('Ownable: caller is not the owner');
        });
    });

    describe('onlyAdmin modifier', () => {
        it('should allow the admin to perform a function with the onlyAdmin modifier', async () => {
          await instance.connect(owner).setAdmin(whitelistedUser.address);
          await instance.connect(whitelistedUser).updateContractSettings(true, false, false);
          result = await instance.connect(whitelistedUser).isPublicMintingEnabled()

          // Perform assertions on the result as needed
          expect(result).to.be.true;
        });

        it('should revert when a non-admin tries to perform a function with the onlyAdmin modifier', async () => {
          // Call the function from a non-admin account and expect it to revert
          await expect(
            instance.connect(whitelistedUser2).updateContractSettings(true, false, false)
          ).to.be.revertedWith('Caller is not admin or owner');
        });
    });

    describe("metadata URI", function () {
        beforeEach(async () => {
          // Mint a treasure chest for testing
          await instance.connect(owner).updateContractSettings(true, false, false);
          await instance.connect(whitelistedUser).mint([]);
        });

        it("should return the correct metadata URI for a minted treasure chest", async function () {
          const tokenId = 1;
          const expectedURI = `${baseURIpath}chest/1/1`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);
        });

        it("should return the correct metadata URI after leveling up a treasure chest", async function () {
          const tokenId = 1;
          const targetLevel = 2;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          const expectedURI = `${baseURIpath}chest/${targetLevel}/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);
        });

        it("should return the correct metadata URI after opening a treasure chest", async function () {
          const tokenId = 1;
          await instance.connect(owner).updateContractSettings(false, false, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/1/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "1");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 2", async function () {
          const tokenId = 1;
          const targetLevel = 2;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/2/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "2");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 3", async function () {
          const tokenId = 1;
          const targetLevel = 3;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/3/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "3");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 4", async function () {
          const tokenId = 1;
          const targetLevel = 4;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/3/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "4");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 5", async function () {
          const tokenId = 1;
          const targetLevel = 5;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/4/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "5");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 5", async function () {
          const tokenId = 1;
          const targetLevel = 5;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/4/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "5");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 6", async function () {
          const tokenId = 1;
          const targetLevel = 6;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/4/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "6");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 7", async function () {
          const tokenId = 1;
          const targetLevel = 7;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/5/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "7");
        });

        it("should return the correct metadata URI after opening a treasure chest lev 8", async function () {
          const tokenId = 1;
          const targetLevel = 8;

          // Level up the treasure chest
          const targetLevelPrice = await instance.levelUpPrice(targetLevel);
          await instance.connect(whitelistedUser).levelUp(tokenId, targetLevel, { value: targetLevelPrice.toString() });

          await instance.connect(owner).updateContractSettings(true, true, true);
          // Open the treasure chest
          const tx = await instance.connect(whitelistedUser).open(tokenId);
          console.log("events", tx.events);
          const expectedURI = `${baseURIpath}gem/5/${tokenId}`;
          const metadataURI = await instance.tokenURI(tokenId);
          expect(metadataURI).to.equal(expectedURI);

          expect(tx)
          .to.emit(instance, "ChestOpened")
          .withArgs(tokenId, whitelistedUser.address, "8");
        });




        // chestGemLevels[1] = 1;
        // chestGemLevels[2] = 2;
        // chestGemLevels[3] = 3;
        // chestGemLevels[4] = 3;
        // chestGemLevels[5] = 4;
        // chestGemLevels[6] = 4;
        // chestGemLevels[7] = 5;
        // chestGemLevels[8] = 5;



      });

    describe("levelUpPrice", function () {
      it("should return the correct level up prices", async function () {
        const levelInfo = await instance.getLevelInfo();
        const levelUpPrices = levelInfo[0];
        const expectedPrices = [6, 17, 35, 60, 90, 120, 155].map((price) => ethers.utils.parseEther(price.toString()));

        for (let i = 0; i < expectedPrices.length; i++) {
          expect(levelUpPrices[i]).to.equal(expectedPrices[i]);
        }
      });
    });

    describe("levelUp", function () {
      beforeEach(async () => {
        // Mint a treasure chest as a whitelisted user
        await instance.connect(owner).updateContractSettings(true, false, false);

      });

      it("should return token info for the tokens owned by each user", async function () {
          // Mint some treasure chests for different users
          const user1 = whitelistedUsers[10];
          const user2 = whitelistedUsers[11];
          const user3 = whitelistedUsers[12];
          await instance.connect(user1).mint([]);
          await instance.connect(user2).mint([]);
          await instance.connect(user3).mint([]);

          // Level up the treasure chests for each user
          await instance.connect(user1).levelUp(1, 2, { value: ethers.utils.parseEther("6") });
          await instance.connect(user2).levelUp(2, 3, { value: ethers.utils.parseEther("17") });
          await instance.connect(user3).levelUp(3, 4, { value: ethers.utils.parseEther("35") });

          console.log("Tokens levled up");
          await instance.connect(owner).updateContractSettings(false, false, true);
          // Open some treasure chests for each user
          const tx2 = await instance.connect(user2).open(2);
          const tx3 = await instance.connect(user3).open(3);

          expect(tx2)
          .to.emit(instance, "ChestOpened")
          .withArgs(2, user2.address, "3");

          expect(tx3)
          .to.emit(instance, "ChestOpened")
          .withArgs(3, user3.address, "4");

          // Get token info list for the tokens owned by each user
          const tokenInfoList1 = await instance.connect(user1).getTokenInfoList(user1.address);
          const tokenInfoList2 = await instance.connect(user2).getTokenInfoList(user2.address);
          const tokenInfoList3 = await instance.connect(user3).getTokenInfoList(user3.address);

          console.log("tokenInfoList1", tokenInfoList1);

          // Assert the token info for each user's owned token
          expect(tokenInfoList1.length).to.equal(1);
          expect(tokenInfoList1[0].tokenId).to.equal(1);
          expect(tokenInfoList1[0].state).to.equal("chest");
          expect(tokenInfoList1[0].level).to.equal(2);

          expect(tokenInfoList2.length).to.equal(1);
          expect(tokenInfoList2[0].tokenId).to.equal(2);
          expect(tokenInfoList2[0].state).to.equal("gem");
          expect(tokenInfoList2[0].level).to.equal(3);

          expect(tokenInfoList3.length).to.equal(1);
          expect(tokenInfoList3[0].tokenId).to.equal(3);
          expect(tokenInfoList3[0].state).to.equal("gem");
          expect(tokenInfoList3[0].level).to.equal(4);

        });

      it("should update ownedTokens when transferring tokens", async function () {
          // Mint a token and transfer it from owner to another user
          await instance.connect(whitelistedUser).mint(emptyProof);
          const ownerTokensBefore = await instance.getOwnedTokens(whitelistedUser.address);
          const tokenId = ownerTokensBefore[0];
          const newOwner = whitelistedUser2.address;
          await instance.connect(whitelistedUser).transferFrom(whitelistedUser.address, newOwner, tokenId);

          // Check the ownedTokens array for the original owner
          const ownerTokens = await instance.getOwnedTokens(whitelistedUser.address);
          expect(ownerTokens).to.not.contain(tokenId);

          // Check the ownedTokens array for the new owner
          const newOwnerTokens = await instance.getOwnedTokens(newOwner);
          console.log("newOwnerTokens", newOwnerTokens);
          console.log("newOwnerTokens", newOwnerTokens[0].toString());
          expect(newOwnerTokens[0].toString()).to.equal(tokenId.toString());
      });



      it("should increase the level of the treasure chest", async function () {
        // Level up the treasure chest to level 2
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await instance.connect(whitelistedUser).levelUp(1, 2, { value: web3.utils.toWei("6", "ether")});
        const chest = await instance.treasureChests(1);
        expect(chest.level).to.equal(2);
      });

      it("should revert when trying to level up to a lower level", async function () {
        // Attempt to level down the treasure chest
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await expect(instance.connect(whitelistedUser).levelUp(1, 1, { value: web3.utils.toWei("6", "ether")})).to.be.revertedWith(
          "Target level should be higher than current level"
        );
        const chest = await instance.treasureChests(1);
        expect(chest.level).to.equal(1);
      });

      it("should revert when trying to level up beyond the maximum level", async function () {
        // Attempt to level up the treasure chest beyond the maximum level
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await expect(instance.connect(whitelistedUser).levelUp(1, MAX_LEVEL + 1)).to.be.revertedWith("Max level is 8");

        const chest = await instance.treasureChests(1);
        expect(chest.level).to.equal(1);
      });

      it("should increase the levelCount for the new level and decrease the levelCount for the previous level", async function () {
        // Level up the treasure chest to level 2
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await instance.connect(whitelistedUser).levelUp(1, 2, { value: web3.utils.toWei("6", "ether")});

        // Check the levelCount for level 1 and level 2
        expect(await instance.levelCount(1)).to.equal(0);
        expect(await instance.levelCount(2)).to.equal(1);
      });

    it("should revert when trying to level up to a fully minted level", async function () {
      // Mint 10 treasure chests and upgrade each one to level 7
      for (let i = 0; i < 10; i++) {
        const user = whitelistedUsers[i];
        await instance.connect(user).mint(emptyProof);
        const tokenId = await instance.totalSupply();
        await instance.connect(user).levelUp(tokenId, 7, { value: pricesInWei[5] });
        const chest = await instance.treasureChests(tokenId);
        expect(chest.level).to.equal(7);
      }

      // Try to mint a new treasure chest for the 11th user and level up to level 7 (should revert)

      await instance.connect(whitelistedUser2).mint(emptyProof);
      const tokenId = await instance.totalSupply();
      await expect(instance.connect(whitelistedUser2).levelUp(tokenId, 7, { value: pricesInWei[5] })).to.be.revertedWith("Next level is fully minted");
      const chest = await instance.treasureChests(tokenId);
      expect(chest.level).to.equal(1);
    });

    it("should successfully level up to each level with dynamic prices", async function () {
      // Mint a treasure chest
      const user = whitelistedUsers[12];
      await instance.connect(user).mint(emptyProof);

      expect(await instance.connect(user).levelCount(1)).to.equal(1);

      // Level up from level 1 to level 2
      const level1Price = await instance.levelUpPrice(2);
      await instance.connect(user).levelUp(1, 2, { value: level1Price });
      expect(await instance.connect(user).levelCount(2)).to.equal(1);
      expect(await instance.connect(user).levelCount(1)).to.equal(0);

      // Level up from level 2 to level 3
      const level2Price = await instance.levelUpPrice(3);
      await instance.connect(user).levelUp(1, 3, { value: (level2Price - level1Price).toString() });
      expect(await instance.connect(user).levelCount(3)).to.equal(1);
      expect(await instance.connect(user).levelCount(2)).to.equal(0);

      // Level up from level 3 to level 4
      const level3Price = await instance.levelUpPrice(4);
      await instance.connect(user).levelUp(1, 4, { value: (level3Price - level2Price).toString() });
      expect(await instance.connect(user).levelCount(4)).to.equal(1);
      expect(await instance.connect(user).levelCount(3)).to.equal(0);

      // Level up from level 4 to level 5
      const level4Price = await instance.levelUpPrice(5);
      await instance.connect(user).levelUp(1, 5, { value: (level4Price - level3Price).toString() });
      expect(await instance.connect(user).levelCount(5)).to.equal(1);
      expect(await instance.connect(user).levelCount(4)).to.equal(0);

      // Level up from level 5 to level 6
      const level5Price = await instance.levelUpPrice(6);
      await instance.connect(user).levelUp(1, 6, { value: (level5Price - level4Price).toString() });
      expect(await instance.connect(user).levelCount(6)).to.equal(1);
      expect(await instance.connect(user).levelCount(5)).to.equal(0);

      // Level up from level 6 to level 7
      const level6Price = await instance.levelUpPrice(7);
      await instance.connect(user).levelUp(1, 7, { value: (level6Price - level5Price).toString() });
      expect(await instance.connect(user).levelCount(7)).to.equal(1);
      expect(await instance.connect(user).levelCount(6)).to.equal(0);

      // Level up from level 7 to level 8
      const level7Price = await instance.levelUpPrice(8);
      await instance.connect(user).levelUp(1, 8, { value: (level7Price - level6Price).toString() });
      expect(await instance.connect(user).levelCount(8)).to.equal(1);
      expect(await instance.connect(user).levelCount(7)).to.equal(0);

      const chest = await instance.connect(user).treasureChests(1);
      expect(chest.level).to.equal(8);
    });

      it("should revert when the payment amount is not exact", async function () {
        // Attempt to level up the treasure chest with incorrect payment amount
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await expect(instance.connect(whitelistedUser).levelUp(1, 2, { value: ethers.utils.parseEther("0.01") })).to.be.revertedWith(
          "Payment must be exact"
        );
        const chest = await instance.treasureChests(1);
        expect(chest.level).to.equal(1);
      });

      it("should increase the level of the treasure chest to the maximum level", async function () {
        // Level up the treasure chest to the maximum level
        await instance.connect(whitelistedUser).mint(emptyProof);
        expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
        expect(await instance.totalSupply()).to.equal(1);
        await instance.connect(whitelistedUser).levelUp(1, MAX_LEVEL, { value: pricesInWei[6] });
        const chest = await instance.treasureChests(1);
        expect(chest.level).to.equal(MAX_LEVEL);
      });
      it("should revert when caller is not the token owner", async function () {
          // Mint a treasure chest
          await instance.connect(whitelistedUsers[0]).mint([]);

          // Level up the treasure chest to level 2
          const tokenId = await instance.totalSupply();
          await instance.connect(whitelistedUsers[0]).levelUp(tokenId, 2, { value: pricesInWei[0] });

          // Try to level up the same treasure chest using a different user (should revert)
          await expect(instance.connect(whitelistedUsers[1]).levelUp(tokenId, 3, { value: pricesInWei[1] })).to.be.revertedWith("Caller is not token owner");
      });

      it("should revert when token ID does not exist", async function () {
          // Mint a treasure chest
          await instance.connect(whitelistedUsers[0]).mint([]);

          // Try to level up a non-existent token ID (should revert)
          const nonExistentTokenId = await instance.totalSupply() + 1;
          await expect(instance.connect(whitelistedUsers[0]).levelUp(nonExistentTokenId, 2, { value: pricesInWei[0] })).to.be.revertedWith("Token ID does not exist");
      });

      it("should revert when trying to level up an opened chest when open phase is not enabled", async function () {
          // Mint a treasure chest
          await instance.connect(nonWhitelistedUser).mint(emptyProof);
          const ExistentTokenId = await instance.totalSupply();

          // Enable the open phase
          await instance.connect(owner).updateContractSettings(false, false, true);

          // Open the treasure chest
          await instance.connect(nonWhitelistedUser).open(ExistentTokenId);

          // Try to level up the opened treasure chest when open phase is not enabled (should revert)
          await expect(instance.connect(nonWhitelistedUser).levelUp(ExistentTokenId, 2, { value: pricesInWei[0] })).to.be.revertedWith("Chest is already opened");
      });

      it("should allow the contract owner to withdraw the contract balance", async function () {
          // Mint a treasure chest
          await instance.connect(whitelistedUser).mint(emptyProof);

          const level1Price = await instance.levelUpPrice(2);
          await instance.connect(whitelistedUser).levelUp(1, 2, { value: level1Price });

          // Level up from level 2 to level 3
          const level2Price = await instance.levelUpPrice(3);

          await instance.connect(whitelistedUser).levelUp(1, 3, { value: (level2Price - level1Price).toString() });

          // Get the initial balance of the contract
          const initialBalance = await ethers.provider.getBalance(instance.address);


          // Withdraw the contract balance
          await instance.withdraw(owner.address);

          // Get the final balance of the contract
          const finalBalance = await ethers.provider.getBalance(instance.address);
          expect(finalBalance).to.equal(0);

          // Check that the contract balance has been transferred to the owner
          const ownerBalance = await ethers.provider.getBalance(owner.address);

          expect(ownerBalance).to.be.above("10016000000000000000000");
      });


      //
      // it("should emit ChestLeveledUp event", async function () {
      //   // Level up the treasure chest to level 2
      //   const levelUpTx = await instance.connect(whitelistedUser).levelUp(1, 2);
      //
      //   // Check the emitted event
      //   expect(levelUpTx)
      //     .to.emit(instance, "ChestLeveledUp")
      //     .withArgs(1, whitelistedUser.address, 2, levelUpPrice[2]);
      // });
    });


    describe("mint", function () {
        it("should allow whitelisted user to mint a treasure chest", async function () {
            await instance.connect(owner).updateContractSettings(false, true, false);
            await instance.connect(whitelistedUser).mint(merkleInfo.proofs[0].proof);
            expect(await instance.isMinted(whitelistedUser.address)).to.equal(true);
            expect(await instance.totalSupply()).to.equal(1);
        });

        it("should revert when non-whitelisted user tries to mint a treasure chest", async function () {
            await instance.connect(owner).updateContractSettings(false, true, false);
            await expect(instance.connect(nonWhitelistedUser).mint(emptyProof)).to.be.revertedWith("Invalid proof");
            expect(await instance.isMinted(nonWhitelistedUser.address)).to.equal(false);
            expect(await instance.totalSupply()).to.equal(0);
        });
        it("should revert when a whitelisted user tries to mint multiple times", async function () {
            await instance.connect(owner).updateContractSettings(false, true, false);
            await instance.connect(whitelistedUser).mint(merkleInfo.proofs[0].proof);
            await expect(instance.connect(whitelistedUser).mint(merkleInfo.proofs[0].proof)).to.be.revertedWith("You can only mint once");

            // Additional assertions if necessary
        });
        it("should emit a Minted event when a treasure chest is minted", async function () {
            await instance.connect(owner).updateContractSettings(false, true, false);
            const mintTx = await instance.connect(whitelistedUser).mint(merkleInfo.proofs[0].proof);
            const receipt = await mintTx.wait();
            expect(receipt.events.length).to.equal(2);
            expect(receipt.events[1].event).to.equal("Minted");
            expect(receipt.events[1].args.tokenId).to.equal(1);
            expect(receipt.events[1].args.user).to.equal(whitelistedUser.address);
        });

        it("should increase levelCount after minting a treasure chest", async function () {
          // Enable public minting
          await instance.connect(owner).updateContractSettings(true, false, false);

          // Mint a treasure chest as a non-whitelisted user
          await instance.connect(nonWhitelistedUser).mint(emptyProof);
          expect(await instance.isMinted(nonWhitelistedUser.address)).to.equal(true);
          expect(await instance.totalSupply()).to.equal(1);

          // Check the levelCount for each level
          for (let i = 1; i <= 8; i++) {
            const levelCount = await instance.levelCount(i);
            if (i === 1) {
              expect(levelCount).to.equal(1);
            } else {
              expect(levelCount).to.equal(0);
            }
          }
        });

        it("should allow public minting when enabled", async function () {
            // Enable public minting
            await instance.connect(owner).updateContractSettings(true, false, false);

            // Mint a treasure chest as a non-whitelisted user
            await instance.connect(nonWhitelistedUser).mint(emptyProof);
            expect(await instance.isMinted(nonWhitelistedUser.address)).to.equal(true);
            expect(await instance.totalSupply()).to.equal(1);

            // Additional assertions if necessary
          });

          it("should revert when public minting is disabled", async function () {
            // Disable public minting
            await instance.connect(owner).updateContractSettings(false, false, false);

            // Attempt to mint a treasure chest as a non-whitelisted user
            await expect(instance.connect(nonWhitelistedUser).mint(emptyProof)).to.be.revertedWith("Public minting is not enabled");
            expect(await instance.isMinted(nonWhitelistedUser.address)).to.equal(false);
            expect(await instance.totalSupply()).to.equal(0);

            // Additional assertions if necessary
          });

    });




});
