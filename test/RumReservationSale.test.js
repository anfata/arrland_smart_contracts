const { ethers, upgrades } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");
const { generateProof } = require("./merkleUtils");

use(solidity);

describe("RumReservationSale contract", function () {
    const supply = ethers.utils.parseUnits("100000000000000", 6);
    let instance;
    let usdc;
    let owner, whitelistedUser, nonWhitelistedUser, withdrawAddress;
    let merkleInfo;
    let merkleProof;
    let merkleProofAddreses;
    let usdcInstance;

    const MAX_USDC_PER_WALLET = ethers.utils.parseUnits("1000", 6);
    const AMOUNT_100_USDC = ethers.utils.parseUnits("100", 6);
    const AMOUNT_900_USDC = ethers.utils.parseUnits("900", 6);
    const AMOUNT_1000_USDC = ethers.utils.parseUnits("1000", 6);
    const AMOUNT_1100_USDC = ethers.utils.parseUnits("1100", 6);
    const WHITELISTED_PRICE = 23 * (10**6); // 0.023 USD 23 * (10 ** usdc.decimals())
    const PUBLIC_PRICE = 24 * (10**6); // 0.024 USD
    const arbitraryRoot = "0x0000000000000000000000000000000000000000000000000000000000000000";
    const emptyProof = [];

    beforeEach(async () => {
        [owner, whitelistedUser, nonWhitelistedUser, withdrawAddress] = await ethers.getSigners();

        const Token = await ethers.getContractFactory("Token");
        usdcInstance = await Token.deploy("USDC", "USDC", supply);

        const factory = await ethers.getContractFactory("RumReservationSale");
        instance = await upgrades.deployProxy(factory, [usdcInstance.address, withdrawAddress.address], { initializer: "initialize" });

        merkleProofAddreses = [whitelistedUser];
        merkleInfo = generateProof(merkleProofAddreses.map(address => address.address));

        await usdcInstance
            .connect(whitelistedUser)
            .approve(instance.address, supply);
        usdcInstance.transfer(whitelistedUser.address, AMOUNT_1100_USDC);
        await usdcInstance
            .connect(nonWhitelistedUser)
            .approve(instance.address, supply);
        usdcInstance.transfer(nonWhitelistedUser.address, AMOUNT_1100_USDC);
        await instance.connect(owner).whiteListUser(merkleInfo.rootHash);
    });

    describe("getAllPurchasingAddresses", function () {
        it("Should return a list of purchasing addresses", async function () {
            // Let two users buy tokens
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof);
            await instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc, emptyProof);

            const purchasingAddresses = await instance.getAllPurchasingAddresses();
            expect(purchasingAddresses).to.be.an("array");
            expect(purchasingAddresses).to.include(nonWhitelistedUser.address);
            expect(purchasingAddresses).to.include(whitelistedUser.address);
        });

        it("Should return an empty array if no users have purchased tokens", async function () {
            const purchasingAddresses = await instance.getAllPurchasingAddresses();
            expect(purchasingAddresses).to.be.an("array");
            expect(purchasingAddresses.length).to.equal(0);
        });
    });

    describe("setPrices", function () {
        it("Should be able to update prices by the owner", async function () {
            const newPricePerToken = ethers.utils.parseUnits("2", await usdcInstance.decimals());
            const newPricePerToken2 = ethers.utils.parseUnits("3", await usdcInstance.decimals());
            await instance.connect(owner).setPrices(newPricePerToken, newPricePerToken2);

            const updatedPricePerToken = await instance.WHITELISTED_PRICE();
            expect(updatedPricePerToken).to.equal(newPricePerToken);

            const updatedPricePerToken2 = await instance.PUBLIC_PRICE();
            expect(updatedPricePerToken2).to.equal(newPricePerToken2);
        });

        it("Should not be able to update prices by non-owner", async function () {
            const newPricePerToken = ethers.utils.parseUnits("2", await usdcInstance.decimals());
            const newPricePerToken2 = ethers.utils.parseUnits("3", await usdcInstance.decimals());

            await expect(instance.connect(nonWhitelistedUser).setPrices(newPricePerToken, newPricePerToken2)).to.be.revertedWith("Ownable: caller is not the owner");
        });

        it("Should revert when setting wrong formatt", async function () {
            const zeroPrice = ethers.constants.Zero;
            await expect(instance.connect(owner).setPrices(2, 2)).to.be.revertedWith("Whitelisted price is not in the correct format.");
            await expect(instance.connect(owner).setPrices(ethers.utils.parseUnits("2", await usdcInstance.decimals()), 2)).to.be.revertedWith("Public price is not in the correct format.");
        });
    });

    describe("test", function () {
        it("Should be able to whitelist users", async function () {
            expect(await instance.merkleRoots(merkleInfo.rootHash)).to.be.true;
        });

        it("Should not be able to whitelist users if not owner", async function () {
            await expect(instance.connect(whitelistedUser).whiteListUser(merkleInfo.rootHash)).to.be.revertedWith("Ownable: caller is not the owner");
        });

        it("Whitelisted user should be able to buy RUM tokens", async function () {
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc);
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof);
            expect(await instance.totalUsdcTransfer(whitelistedUser.address)).to.equal(amountOfUsdc);
        });

        it("Whitelisted user should get WHITELISTED_PRICE and have more tokens", async function () {
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());

            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc);

            // Perform the buyRumToken function for the whitelisted user
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof);

            // Calculate the expected amount of RUM tokens the whitelisted user should receive
            const expectedRumTokens = amountOfUsdc.mul(10 ** await usdcInstance.decimals()).div(WHITELISTED_PRICE);

            // Retrieve the user's purchase from the smart contract
            const userPurchases = await instance.getPurchasesByUser(whitelistedUser.address);
            const lastPurchase = userPurchases[userPurchases.length - 1];
            // Compare the expected amount of tokens with the actual amount of tokens received by the user
            expect(lastPurchase.rumTokens.toNumber()/1000).to.equal(4347.826);
            expect(expectedRumTokens.toNumber()/1000).to.equal(4347.826);
        });


        it("Non-whitelisted user should get PUBLIC_PRICE and have less tokens", async function () {
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());

            await usdcInstance.connect(nonWhitelistedUser).approve(instance.address, amountOfUsdc);

            // Perform the buyRumToken function for the non-whitelisted user
            await instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc, emptyProof);

            // Calculate the expected amount of RUM tokens the non-whitelisted user should receive
            const expectedRumTokens = amountOfUsdc.mul(10 ** await usdcInstance.decimals()).div(PUBLIC_PRICE);

            // Retrieve the user's purchase from the smart contract
            const userPurchases = await instance.getPurchasesByUser(nonWhitelistedUser.address);
            const lastPurchase = userPurchases[userPurchases.length - 1];

            // Compare the expected amount of tokens with the actual amount of tokens received by the user
            expect(lastPurchase.rumTokens.toNumber()/1000).to.equal(4166.666);
            expect(expectedRumTokens.toNumber()/1000).to.equal(4166.666);
        });

        it("Should not be able to buy more than the limit for whitelisted user", async function () {
             const amountOfUsdc = ethers.utils.parseUnits("1001", await usdcInstance.decimals());
             await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc);
             await expect(instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof)).to.be.revertedWith("Exceeds limit.");
        });

        it("Should not be able to buy more than the limit for whitelisted user with multiple buys", async function () {
             const amountOfUsdc0 = ethers.utils.parseUnits("1001", await usdcInstance.decimals());
             const amountOfUsdc1 = ethers.utils.parseUnits("900", await usdcInstance.decimals());
             const amountOfUsdc2 = ethers.utils.parseUnits("100", await usdcInstance.decimals());
             const amountOfUsdc3 = ethers.utils.parseUnits("1", await usdcInstance.decimals());
             await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc0);
             await expect(instance.connect(whitelistedUser).buyRumToken(amountOfUsdc1, merkleInfo.proofs[0].proof)).not.to.be.revertedWith("Exceeds limit.");
             await expect(instance.connect(whitelistedUser).buyRumToken(amountOfUsdc2, merkleInfo.proofs[0].proof)).not.to.be.revertedWith("Exceeds limit.");
             await expect(instance.connect(whitelistedUser).buyRumToken(amountOfUsdc3, merkleInfo.proofs[0].proof)).to.be.revertedWith("Exceeds limit.");
        });

        it("Should not be able to buy more than the limit for non whitelisted user", async function () {
             const amountOfUsdc = ethers.utils.parseUnits("1001", await usdcInstance.decimals());
             await usdcInstance.connect(nonWhitelistedUser).approve(instance.address, amountOfUsdc);
             await expect(instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc, emptyProof)).to.be.revertedWith("Exceeds limit.");
        });

        it("Should not be able to buy more than the limit for non whitelisted user with multiple buys", async function () {
             const amountOfUsdc0 = ethers.utils.parseUnits("1001", await usdcInstance.decimals());
             const amountOfUsdc1 = ethers.utils.parseUnits("900", await usdcInstance.decimals());
             const amountOfUsdc2 = ethers.utils.parseUnits("100", await usdcInstance.decimals());
             const amountOfUsdc3 = ethers.utils.parseUnits("1", await usdcInstance.decimals());
             await usdcInstance.connect(nonWhitelistedUser).approve(instance.address, amountOfUsdc0);
             await expect(instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc1, emptyProof)).not.to.be.revertedWith("Exceeds limit.");
             await expect(instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc2, emptyProof)).not.to.be.revertedWith("Exceeds limit.");
             await expect(instance.connect(nonWhitelistedUser).buyRumToken(amountOfUsdc3, emptyProof)).to.be.revertedWith("Exceeds limit.");
        });

        it("Two whitelisted users should be able to buy RUM tokens in sequence", async function () {
            // First user tries to buy
            const amountOfUsdc1 = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc1);
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc1, merkleInfo.proofs[0].proof);
            expect(await instance.totalUsdcTransfer(whitelistedUser.address)).to.equal(amountOfUsdc1);

            // Whitelist a new user
            const newWhitelistedUser = nonWhitelistedUser;
            const newMerkleInfo = generateProof([newWhitelistedUser].map(address => address.address));
            console.log()
            await instance.connect(owner).whiteListUser(newMerkleInfo.rootHash);
            expect(await instance.merkleRoots(newMerkleInfo.rootHash)).to.be.true;

            // New user tries to buy
            const amountOfUsdc2 = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await usdcInstance.connect(newWhitelistedUser).approve(instance.address, amountOfUsdc2);
            await instance.connect(newWhitelistedUser).buyRumToken(amountOfUsdc2, newMerkleInfo.proofs[0].proof);
            expect(await instance.totalUsdcTransfer(newWhitelistedUser.address)).to.equal(amountOfUsdc2);

            // First user tries to buy again
            const amountOfUsdc3 = ethers.utils.parseUnits("50", await usdcInstance.decimals());
            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc3);
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc3, merkleInfo.proofs[0].proof);
            expect(await instance.totalUsdcTransfer(whitelistedUser.address)).to.equal(amountOfUsdc1.add(amountOfUsdc3));
        });

        it("Owner should be able to withdraw USDC", async function () {
            // Buy some RUM tokens to have USDC in the contract
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc);
            await instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof);

            // Check initial USDC balance of the withdrawAddress
            const initialBalance = await usdcInstance.balanceOf(withdrawAddress.address);

            // Owner withdraws USDC from the contract
            await instance.connect(owner).withdrawUsdcToken();

            // Check final USDC balance of the withdrawAddress
            const finalBalance = await usdcInstance.balanceOf(withdrawAddress.address);

            // Expect the final balance to be greater than the initial balance
            expect(finalBalance).to.be.gt(initialBalance);
        });
        it("Non-owner should not be able to withdraw USDC", async function () {
            await expect(instance.connect(whitelistedUser).withdrawUsdcToken()).to.be.revertedWith("Ownable: caller is not the owner");
        });

        it("Owner should be able to pause the contract", async function () {
            await instance.connect(owner).pause();
            expect(await instance.paused()).to.be.true;
        });

        it("Owner should be able to unpause the contract", async function () {
            await instance.connect(owner).pause();
            await instance.connect(owner).unPause();
            expect(await instance.paused()).to.be.false;
        });

        it("Non-owner should not be able to pause or unpause the contract", async function () {
            await expect(instance.connect(whitelistedUser).pause()).to.be.revertedWith("Ownable: caller is not the owner");
            await expect(instance.connect(whitelistedUser).unPause()).to.be.revertedWith("Ownable: caller is not the owner");
        });

        it("Should not be able to buy RUM tokens when paused", async function () {
            await instance.connect(owner).pause();
            const amountOfUsdc = ethers.utils.parseUnits("100", await usdcInstance.decimals());
            await usdcInstance.connect(whitelistedUser).approve(instance.address, amountOfUsdc);
            await expect(instance.connect(whitelistedUser).buyRumToken(amountOfUsdc, merkleInfo.proofs[0].proof)).to.be.revertedWith("Pausable: paused");
            await instance.connect(owner).unPause();
        });

        it("isWhitelisted should return the correct whitelist status for a user", async function () {
            // Check if the whitelisted user is marked as whitelisted
            const isWhitelistedUserWhitelisted = await instance.isWhitelisted(whitelistedUser.address,  merkleInfo.proofs[0].proof);
            expect(isWhitelistedUserWhitelisted).to.be.true;

            // Check if the non-whitelisted user is marked as not whitelisted
            const isNonWhitelistedUserWhitelisted = await instance.isWhitelisted(nonWhitelistedUser.address, emptyProof);
            expect(isNonWhitelistedUserWhitelisted).to.be.false;
        });
    });
});
