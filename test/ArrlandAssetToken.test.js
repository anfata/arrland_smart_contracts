const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");
const { ethers } = require("hardhat");
const truffleAssert = require("truffle-assertions");

use(solidity);

describe("ArrlandAssetToken", () => {
    let owner, wallet1, wallet2, wallet3, token, factory, burnDelay;

    beforeEach(async () => {
        [owner, wallet1, wallet2, wallet3] = await ethers.getSigners();     
        factory = await ethers.getContractFactory("ArrlandAssetToken");
        token = await factory.deploy("ArrlandAssetToken", "AAT", 30 * 24 * 60 * 60);
        burnDelay = parseInt(await token.burnDelay());
    });

    describe("transferFrom", () => {
        it("should set mint timestamp to the current block timestamp if the sender's address is not in mintTimestamps", async () => {
            // Mint some tokens to user X
            await token.connect(owner).mint(wallet1.address, 100);

            const mintTimestampWallet1 = await token.getMintTimestamp(wallet1.address);

            // Create a new contract that calls transfer function
            const TestContract = await ethers.getContractFactory("TestContract", owner);
            const testContract = await TestContract.deploy(token.address);
            await testContract.deployed();
    
            // Approve the TestContract to spend tokens from user X
            await token.connect(wallet1).approve(testContract.address, 100);
    
            // Call transferFrom function of the TestContract to transfer tokens from user X to TestContract address
            await testContract.connect(wallet1).testPayment(50);
    
            expect(await token.balanceOf(testContract.address)).to.eq(50);
            // Check the mint timestamp for TestContract address
            const mintTimestamp = await token.getMintTimestamp(testContract.address);
            expect(mintTimestamp.toNumber()).to.eq(mintTimestampWallet1.toNumber());
        });
        it("should burn tokens correctly after redistribute and transfer", async () => {
            // Mint some tokens to user X
            await token.connect(owner).mint(wallet1.address, 100);

            const mintTimestampWallet1 = await token.getMintTimestamp(wallet1.address);

            // Create a new contract that calls transfer function
            const TestContract = await ethers.getContractFactory("TestContract", owner);
            const testContract = await TestContract.deploy(token.address);
            await testContract.deployed();
    
            // Approve the TestContract to spend tokens from user X
            await token.connect(wallet1).approve(testContract.address, 100);
    
            // Call transferFrom function of the TestContract to transfer tokens from user X to TestContract address
            await testContract.connect(wallet1).testPayment(50);
            expect(await token.balanceOf(testContract.address)).to.eq(50);
            await ethers.provider.send("evm_increaseTime", [burnDelay]);

            // Call burn_all_tokens
            await token.connect(owner).burn_all_tokens();
          
            // Check balance after burn
            const balanceAfter1 = await token.balanceOf(testContract.address);
            const balanceAfter2 = await token.balanceOf(wallet2.address);
            
            // Check if the tokens were burned
            expect(balanceAfter1).to.eq(0);
            expect(balanceAfter2).to.eq(0);
        });
    });

    describe("redistribute", () => {
        it("should redistribute tokens to the provided addresses", async () => {
            const addresses = [wallet1.address, wallet2.address];
            const counts = [100, 200];

            await token.connect(owner).redistribute(addresses, counts);

            expect(await token.balanceOf(wallet1.address)).to.eq(100);
            expect(await token.balanceOf(wallet2.address)).to.eq(200);
        });

        it("should only be callable by the contract owner", async () => {
            const addresses = [wallet1.address, wallet2.address];
            const counts = [100, 200];

            await truffleAssert.reverts(
                token.connect(wallet1).redistribute(addresses, counts),
                "Ownable: caller is not the owner"
            );
        });

        it("should fail if the addresses and counts arrays have different lengths", async () => {
            const addresses = [wallet1.address, wallet2.address];
            const counts = [100];

            await truffleAssert.reverts(
                token.connect(owner).redistribute(addresses, counts),
                "Addresses and counts arrays must have the same length."
            );
        });

        it("should burn tokens correctly after redistribute and transfer", async () => {
            // Mint some tokens to user X
            await token.connect(owner).mint(wallet1.address, 100);
            
            // Transfer tokens to user Y
            await token.connect(wallet1).transfer(wallet2.address, 100);

            // Fast forward time by burnDelay            
            await ethers.provider.send("evm_increaseTime", [burnDelay]);
            
            // Check balance before burn
            const balanceBefore = await token.balanceOf(wallet2.address);
            expect(balanceBefore).to.eq(100);

            // Call burn_all_tokens
            await token.connect(owner).burn_all_tokens();
          
            // Check balance after burn
            const balanceAfter1 = await token.balanceOf(wallet1.address);
            const balanceAfter2 = await token.balanceOf(wallet2.address);
            
            // Check if the tokens were burned
            expect(balanceAfter1).to.eq(0);
            expect(balanceAfter2).to.eq(0);
        });
          
    });

    describe("burn_all_tokens", () => {
        it("should burn all tokens that all holders have", async () => {
            await token.connect(owner).mint(wallet1.address, 100);
            await token.connect(owner).mint(wallet2.address, 200);

            await ethers.provider.send("evm_increaseTime", [burnDelay]);

            await token.connect(owner).burn_all_tokens();

            expect(await token.balanceOf(wallet1.address)).to.eq(0);
            expect(await token.balanceOf(wallet2.address)).to.eq(0);
        });

        it("should only burn tokens that have been minted for more than the burn delay", async () => {
            await token.connect(owner).mint(wallet1.address, 100);
            

            await ethers.provider.send("evm_increaseTime", [burnDelay]);
            await token.connect(owner).mint(wallet2.address, 200);

            await token.burn_all_tokens();

            expect(await token.balanceOf(wallet1.address)).to.eq(0);
            expect(await token.balanceOf(wallet2.address)).to.eq(200);
        });
      
        it("should only be callable by the contract owner", async () => {
            await truffleAssert.reverts(
                token.connect(wallet1).burn_all_tokens(),
                "Ownable: caller is not the owner"
            );
        });
    });
    describe("mint", () => {
        it("should mint tokens to the provided address", async () => {
            await token.connect(owner).mint(wallet1.address, 100);
            expect(await token.balanceOf(wallet1.address)).to.eq(100);
        });
    });

    describe("transfer", () => {
        it("should transfer tokens from the msg.sender to the provided address", async () => {
            await token.connect(owner).mint(wallet1.address, 100);
            await token.connect(wallet1).transfer(wallet2.address, 50);
            expect(await token.balanceOf(wallet1.address)).to.eq(50);
            expect(await token.balanceOf(wallet2.address)).to.eq(50);
        });
    });
    it("should reset mint timestamp when the balance of the msg.sender is equal to the transferred value", async () => {
        await token.connect(owner).mint(wallet1.address, 15);
        const mintTimestamp = await token.getMintTimestamp(wallet1.address);
        expect(mintTimestamp).to.not.eq(0);
        const initialBalance = await token.connect(wallet1).balanceOf(wallet1.address);
        await token.connect(wallet1).transfer(wallet2.address, initialBalance);
        const newMintTimestamp = await token.getMintTimestamp(wallet1.address);
        expect(newMintTimestamp).to.eq(0);        
        const afterBalance = await token.connect(wallet2).balanceOf(wallet2.address);
        expect(afterBalance).to.eq(15); 
    });
    it("should set mint timestamp to the current block timestamp when mint function is called", async () => {
        const tx = await token.connect(owner).mint(wallet1.address, 100);
        await tx.wait();
        const blockTimestamp = await ethers.provider.getBlock(tx.blockHash);
        const mintTimestamp = await token.getMintTimestamp(wallet1.address);
        expect(mintTimestamp.toNumber()).to.eq(blockTimestamp.timestamp)
    });

});

      
